package com.axinfu.util.filescanner;

/**
 * jar文件里面的file信息
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class File {

    /**
     * 包名
     */
    private String packageName;

    /**
     * 文件名
     */
    private String name;

    /**
     * 路径
     */
    private String path;

    public File(String packageName, String name, String path) {
        this.packageName = packageName;
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getClassName() {
        return this.getPath()
                .replaceFirst("/", "")
                .replaceAll("/", ".")
                .replaceAll(".class", "");
    }

    @Override
    public String toString() {
        return getPath();
    }
}
