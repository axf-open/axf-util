package com.axinfu.util.http.response;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * 返回字符串
 *
 * @author zjn
 * @since 2022/3/23
 */
public class StringResponseHandler implements ResponseHandler<StringHttpResponseResult> {

    private StringHttpResponseResult responseResult;

    public StringResponseHandler() {
    }

    @Override
    public StringHttpResponseResult handler(HttpUriRequest request, HttpResponse response) throws Exception {
        ByteResponseHandler byteResponseHandler = new ByteResponseHandler();
        HttpResponseResult resultBase = byteResponseHandler.handler(request, response);
        responseResult = new StringHttpResponseResult(resultBase);
        return responseResult;
    }

    @Override
    public String buildPrintBody() {
        return responseResult.getResponseStr();
    }
}
