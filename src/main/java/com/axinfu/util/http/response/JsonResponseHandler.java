package com.axinfu.util.http.response;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * 返回json
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class JsonResponseHandler implements ResponseHandler<JsonHttpResponseResult> {

    private JsonHttpResponseResult responseResult;

    public JsonResponseHandler() {
    }

    @Override
    public JsonHttpResponseResult handler(HttpUriRequest request, HttpResponse response) throws Exception {
        StringResponseHandler stringResponseHandler = new StringResponseHandler();
        StringHttpResponseResult resultBase = stringResponseHandler.handler(request, response);
        responseResult = new JsonHttpResponseResult(resultBase);
        return responseResult;
    }

    @Override
    public String buildPrintBody() {
        return responseResult.getResponseStr();
    }
}
