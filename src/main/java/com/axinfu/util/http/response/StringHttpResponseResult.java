package com.axinfu.util.http.response;

import com.axinfu.util.EmptyUtil;

import java.io.UnsupportedEncodingException;

/**
 * http请求返回结果
 *
 * @author zjn
 * @since 2022/3/23
 */
public class StringHttpResponseResult extends HttpResponseResult {

    private String responseStr;

    public StringHttpResponseResult(HttpResponseResult httpResponseResult) {
        setStatus(httpResponseResult.getStatus());
        setRequest(httpResponseResult.getRequest());
        setResponse(httpResponseResult.getResponse());
        setCharset(httpResponseResult.getCharset());
        setResponseByte(httpResponseResult.getResponseByte());
        setHeaderMap(httpResponseResult.getHeaderMap());
        try {
            if (EmptyUtil.isNotEmpty(httpResponseResult.getResponseByte())) {
                this.responseStr = new String(httpResponseResult.getResponseByte(), httpResponseResult.getCharset());
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public String getResponseStr() {
        return responseStr;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" +
                "headerMap=" + getHeaderMap() +
                "responseJson='" + responseStr + '\'' +
                '}';
    }
}
