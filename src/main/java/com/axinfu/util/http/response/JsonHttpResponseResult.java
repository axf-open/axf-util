package com.axinfu.util.http.response;

import com.alibaba.fastjson.JSONObject;

/**
 * http请求返回结果
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class JsonHttpResponseResult extends StringHttpResponseResult {

    private final JSONObject responseJson;

    public JsonHttpResponseResult(StringHttpResponseResult stringHttpResponseResult) {
        super(stringHttpResponseResult);
        this.responseJson = JSONObject.parseObject(stringHttpResponseResult.getResponseStr());
    }

    public JSONObject getResponseJson() {
        return responseJson;
    }
}
