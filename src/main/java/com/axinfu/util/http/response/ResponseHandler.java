package com.axinfu.util.http.response;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

/**
 * HttpResponseHandler
 *
 * @author zjn
 * @since 2022/3/23
 */
public interface ResponseHandler<T extends HttpResponseResult> {

    /**
     * response处理程序
     *
     * @param request  request
     * @param response response
     * @return 处理后结果
     * @throws Exception 异常
     */
    T handler(HttpUriRequest request, HttpResponse response) throws Exception;

    /**
     * 构建body的打印内容
     *
     * @return String
     */
    String buildPrintBody();

}
