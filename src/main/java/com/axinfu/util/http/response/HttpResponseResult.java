package com.axinfu.util.http.response;

import com.axinfu.util.EmptyUtil;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * http请求返回结果
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class HttpResponseResult {

    private int status;
    private HttpUriRequest request;
    private HttpResponse response;
    private String charset = "UTF-8";
    private byte[] responseByte;
    private Map<String, List<String>> headerMap = new HashMap<>();

    public HttpResponseResult() {
    }

    public HttpResponseResult(byte[] responseByte) {
        this.responseByte = responseByte;
    }

    /**
     * 添加header
     *
     * @param key   名称
     * @param value 值
     */
    public void addHeader(String key, String value) {
        key = key.toLowerCase();

        if (headerMap.containsKey(key)) {
            List<String> headerValues = headerMap.get(key);
            if (!headerValues.contains(value)) {
                headerValues.add(value);
            }
            return;
        }

        List<String> headerValuesNew = new ArrayList<>();
        headerValuesNew.add(value);
        headerMap.put(key, headerValuesNew);
    }

    /**
     * 添加一组header
     *
     * @param headers 值
     */
    public void addHeaders(Map<String, List<String>> headers) {
        for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
            String key = entry.getKey();
            for (String value : entry.getValue()) {
                addHeader(key, value);
            }
        }
    }

    /**
     * 获取header（第一个值）
     *
     * @param key 名称
     * @return 值
     */
    public String getHeader(String key) {
        key = key.toLowerCase();
        List<String> values = headerMap.get(key);
        if (EmptyUtil.isNotEmpty(values)) {
            return values.get(0);
        }
        return null;
    }

    /**
     * 获取header
     *
     * @param key 名称
     * @return 值
     */
    public List<String> getHeaderMult(String key) {
        key = key.toLowerCase();
        return headerMap.get(key);
    }

    public InputStream getResponseBody() throws UnsupportedOperationException {
        return new ByteArrayInputStream(responseByte);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public HttpUriRequest getRequest() {
        return request;
    }

    public void setRequest(HttpUriRequest request) {
        this.request = request;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public void setResponse(HttpResponse response) {
        this.response = response;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public byte[] getResponseByte() {
        return responseByte;
    }

    public void setResponseByte(byte[] responseByte) {
        this.responseByte = responseByte;
    }

    public Map<String, List<String>> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(Map<String, List<String>> headerMap) {
        this.headerMap = headerMap;
    }

    @Override
    public String toString() {
        return "HttpResponseResult{" +
                "headerMap=" + headerMap +
                "responseByte='" + responseByte.length + '\'' +
                '}';
    }
}
