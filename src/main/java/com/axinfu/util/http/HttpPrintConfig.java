package com.axinfu.util.http;

import com.axinfu.util.PathUtil;

/**
 * 打印设置
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class HttpPrintConfig {

    private String url;
    private boolean printLog = false;
    private boolean printFile = false;
    private String printFilePath = PathUtil.getRootClassPath();

    public HttpPrintConfig(String url, boolean printLog) {
        this.url = url;
        this.printLog = printLog;
    }

    public HttpPrintConfig(String url, boolean printFile, String printFilePath) {
        this.url = url;
        this.printFile = printFile;
        this.printFilePath = printFilePath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isPrintLog() {
        return printLog;
    }

    public void setPrintLog(boolean printLog) {
        this.printLog = printLog;
    }

    public boolean isPrintFile() {
        return printFile;
    }

    public void setPrintFile(boolean printFile) {
        this.printFile = printFile;
    }

    public String getPrintFilePath() {
        return printFilePath;
    }

    public void setPrintFilePath(String printFilePath) {
        this.printFilePath = printFilePath;
    }
}
