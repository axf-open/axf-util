package com.axinfu.util.config;

import com.alibaba.fastjson.JSONObject;
import com.axinfu.util.config.callback.ApplyCallBack;

import java.util.List;

/**
 * BtgSet
 *
 * @author zjn
 * @since 2022/3/24
 */
@SuppressWarnings("unused")
public class BtgSet {

    private static JSONObject reJson = new JSONObject();

    private BtgSet() {
    }

    public static JSONObject getParams() {
        return reJson;
    }

    public static void init(JSONObject re) {
        JSONObject pre = reJson;
        reJson = re;
        //应用新的参数后执行回调函数
        applyCallBack(pre, reJson);
    }

    public static String getString(String key) {
        return reJson.getString(key);
    }

    public static Boolean getBoolean(String key) {
        return reJson.getBoolean(key);
    }

    public static Integer getInteger(String key) {
        return reJson.getInteger(key);
    }

    /**
     * 应用新的参数后执行回调函数
     *
     * @param dirty 应用前参数
     * @param now   应用后参数
     */
    private static void applyCallBack(JSONObject dirty, JSONObject now) {
        List<ApplyCallBack> applyCallBackList = BtgSetPlugin.getBtgSetPlugin().applyCallBackList;
        for (ApplyCallBack callBack : applyCallBackList) {
            callBack.callBack(dirty, now);
        }
    }
}
