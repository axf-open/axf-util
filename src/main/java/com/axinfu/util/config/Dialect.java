package com.axinfu.util.config;

/**
 * Dialect
 *
 * @author zjn
 * @since 2022/3/24
 */
public enum Dialect {

    /**
     * mysql
     */
    MYSQL,

    /**
     * oracle
     */
    ORACLE

}
