package com.axinfu.util.config.callback;

/**
 * CustomAction
 *
 * @author zjn
 * @since 2022/3/24
 */
public class CustomAction {

    private String btnTitle;
    private String desc;
    private ExcuteCallBack callBack;

    private CustomAction() {
    }

    public CustomAction(String btnTitle, ExcuteCallBack callBack) {
        this.btnTitle = btnTitle;
        this.callBack = callBack;
    }

    public CustomAction(String btnTitle, ExcuteCallBack callBack, String desc) {
        this.btnTitle = btnTitle;
        this.desc = desc;
        this.callBack = callBack;
    }

    public String getBtnTitle() {
        return btnTitle;
    }

    public void setBtnTitle(String btnTitle) {
        this.btnTitle = btnTitle;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ExcuteCallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(ExcuteCallBack callBack) {
        this.callBack = callBack;
    }
}
