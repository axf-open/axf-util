package com.axinfu.util.config.callback;


import com.alibaba.fastjson.JSONObject;

/**
 * ApplyCallBack
 *
 * @author zjn
 * @since 2022/3/24
 */
public interface ApplyCallBack {

    /**
     * callBack
     *
     * @param dirty dirty
     * @param now   now
     */
    void callBack(JSONObject dirty, JSONObject now);
}
