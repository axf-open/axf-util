package com.axinfu.util.config.callback;


/**
 * ExcuteCallBack
 *
 * @author zjn
 * @since 2022/3/24
 */
public interface ExcuteCallBack {

    /**
     * callBack
     */
    void callBack();
}
