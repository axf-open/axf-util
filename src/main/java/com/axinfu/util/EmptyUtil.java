package com.axinfu.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * 工具类:验证是否为空
 *
 * @author zjn
 * @since 2022/3/23
 */
public class EmptyUtil {

    private EmptyUtil() {
    }

    /**
     * 验证数组是否为空
     *
     * @param array 待验证数组
     * @return boolean
     */
    public static boolean isEmpty(Object[] array) {
        return (array == null || array.length == 0);
    }

    /**
     * 验证对象是否为空
     *
     * @param obj 待验证对象
     * @return boolean
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj instanceof Optional) {
            return !((Optional) obj).isPresent();
        }
        if (obj instanceof CharSequence) {
            return ((CharSequence) obj).toString().trim().length() == 0;
        }
        if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        }
        if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        }
        if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        }
        return false;
    }

    /**
     * 验证数组是否不为空
     *
     * @param array 待验证数组
     * @return boolean
     */
    public static boolean isNotEmpty(Object[] array) {
        return !isEmpty(array);
    }

    /**
     * 验证对象是否不为空
     *
     * @param obj 待验证对象
     * @return boolean
     */
    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    /**
     * 验证对象是否为空
     *
     * @param obj 待验证对象
     * @param yes 为true（为空）时的值
     * @param no  为false（不为空）时的值
     * @param <T> T
     * @param <R> R
     * @return T
     */
    public static <T, R> R isEmpty(T obj, R yes, R no) {
        return isEmpty(obj) ? yes : no;
    }

    /**
     * 验证对象是否不为空
     *
     * @param obj 待验证对象
     * @param yes 为true（不为空）时的值
     * @param no  为false（为空）时的值
     * @param <T> T
     * @param <R> R
     * @return T
     */
    public static <T, R> R isNotEmpty(T obj, R yes, R no) {
        return isNotEmpty(obj) ? yes : no;
    }

    /**
     * 判断是否不为空若为空返回默认值
     *
     * @param obj 待验证对象
     * @param dft 为false（为空）时的值
     * @param <T> T
     * @return T
     */
    public static <T> T isNotEmptyOrDefault(T obj, T dft) {
        return isNotEmpty(obj, obj, dft);
    }
}
