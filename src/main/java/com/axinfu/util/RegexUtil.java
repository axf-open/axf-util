package com.axinfu.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式
 *
 * @author ZJN
 * @since 2022/5/7
 */
public class RegexUtil {

    /**
     * 判断是否匹配
     *
     * @param pattern pattern
     * @param content content
     * @return boolean
     */
    public static boolean isMatch(String pattern, String content) {
        return Pattern.matches(pattern, content);
    }

    /**
     * 捕获组
     *
     * @param pattern pattern
     * @param content content
     * @return Matcher
     */
    public static Matcher match(String pattern, String content) {
        Pattern p = Pattern.compile(pattern);
        return p.matcher(content);
    }

    /**
     * 捕获组返回list结构
     *
     * @param pattern pattern
     * @param content content
     * @return List
     */
    public static List<List<String>> matchToList(String pattern, String content) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(content);
        List<List<String>> rs = new ArrayList<>();
        List<String> rsItem;
        while (m.find()) {
            rsItem = new ArrayList<>();
            for (int i = 0, iLen = m.groupCount(); i <= iLen; i++) {
                rsItem.add(m.group(i));
            }
            rs.add(rsItem);
        }
        return rs;
    }
}
