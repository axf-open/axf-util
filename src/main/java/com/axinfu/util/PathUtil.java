package com.axinfu.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;

/**
 * PathUtil
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("rawtypes")
public class PathUtil {

    private PathUtil() {
    }

    /**
     * 获取类路径
     *
     * @param clazz 类
     * @return 路径
     */
    public static String getPath(Class clazz) {
        return getPathByUrl(clazz.getResource(""));
    }

    /**
     * 获取对象类路径
     *
     * @param object 对象
     * @return 路径
     */
    public static String getPath(Object object) {
        URL url = object.getClass().getResource("");
        return getPathByUrl(url);
    }

    /**
     * 获取类包路径
     *
     * @param clazz 类
     * @return 路径
     */
    public static String getPackagePath(Class clazz) {
        Package p = clazz.getPackage();
        return p != null ? p.getName().replaceAll("\\.", "/") : "";
    }

    /**
     * 获取对象包路径
     *
     * @param object 对象
     * @return 路径
     */
    public static String getPackagePath(Object object) {
        Package p = object.getClass().getPackage();
        return p != null ? p.getName().replaceAll("\\.", "/") : "";
    }

    /**
     * 获取class根路径
     * 注意：命令行返回的是命令行所在的当前路径
     *
     * @return 路径
     */
    public static String getRootClassPath() {
        try {
            URL url = getClassLoader().getResource("");
            if (EmptyUtil.isEmpty(url)) {
                throw new NullPointerException();
            }
            String path = url.toURI().getPath();
            return new File(path).getAbsolutePath();
        } catch (Exception e) {
            try {
                String path = PathUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath();
                path = java.net.URLDecoder.decode(path, "UTF-8");
                if (path.endsWith(File.separator)) {
                    path = path.substring(0, path.length() - 1);
                }
                return path;
            } catch (UnsupportedEncodingException e1) {
                throw new RuntimeException(e1);
            }
        }
    }

    /**
     * 是否物理路径绝对路径
     *
     * @param path 路径
     * @return 是或否
     */
    public static boolean isAbsolutePath(String path) {
        return path.startsWith("/") || path.indexOf(':') == 1;
    }

    /**
     * 优先使用 current thread 所使用的 ClassLoader 去获取路径
     * 否则在某些情况下会获取到 tomcat 的 ClassLoader，那么路径值将是
     * TOMCAT_HOME/lib
     */
    private static ClassLoader getClassLoader() {
        ClassLoader ret = Thread.currentThread().getContextClassLoader();
        return ret != null ? ret : PathUtil.class.getClassLoader();
    }

    /**
     * getPath
     *
     * @param url url
     * @return String
     */
    private static String getPathByUrl(URL url) {
        if (EmptyUtil.isEmpty(url)) {
            return null;
        }
        String path = url.getPath();
        return new File(path).getAbsolutePath();
    }
}
