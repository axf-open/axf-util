package com.axinfu.util.validq;

import com.axinfu.util.EmptyUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证器上下文
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class ValidatorContext {
    /**
     * 共享属性
     */
    private Map<String, Object> attrs = new HashMap<>();

    /**
     * 验证结果
     */
    private ValidationResult result = new ValidationResult();

    /**
     * 添加一个错误信息
     *
     * @param errorCode 错误编码
     * @param errorMsg  错误描述
     */
    public void addError(String errorCode, String errorMsg) {
        this.result.addError(errorCode, errorMsg);
    }

    /**
     * 添加一个错误信息
     *
     * @param validationError 错误信息对象
     */
    public void addError(ValidationError validationError) {
        this.result.addError(validationError);
    }

    /**
     * 根据参数名称获取参数值
     *
     * @param key 参数名称
     * @param <T> 值类型
     * @return 参数值
     */
    @SuppressWarnings("unchecked")
    public <T> T getAttr(String key) {
        return (T) attrs.get(key);
    }

    /**
     * 设置参数
     *
     * @param key   参数名称
     * @param value 参数值
     */
    public void setAttr(String key, Object value) {
        this.attrs.put(key, value);
    }

    /**
     * 设置参数
     *
     * @param values 参数
     */
    public void setAttr(Map<String, Object> values) {
        this.attrs.putAll(values);
    }

    /**
     * 替换参数
     *
     * @param str           带占位符${}的字符串
     * @param defaultParams 当上下文没找到时备选的参数列表
     * @return 替换后的字符串
     */
    @SuppressWarnings("all")
    public String replaceParams(String str, Map<String, String> defaultParams) {
        defaultParams = null == defaultParams ? new HashMap<>(1) : defaultParams;
        String pRegStr = "\\$\\{[^\\{\\}]+\\}";
        String pRegStrReplace = "\\\\\\$\\\\{([^\\\\{\\\\}]+)\\\\}";
        String pstr = "^" + str.replaceAll(pRegStr, pRegStrReplace) + "$";
        Pattern p = Pattern.compile(pstr);
        str = str.replace("\\", "");
        Matcher m = p.matcher(str);
        if (m.find()) {
            for (int i = 1, l = m.groupCount(); i <= l; i++) {
                String paramName = m.group(i);
                String paramValue = getAttr(paramName);
                paramValue = EmptyUtil.isNotEmptyOrDefault(paramValue, "");
                paramValue = EmptyUtil.isNotEmptyOrDefault(paramValue, defaultParams.get(paramName));
                str = str.replaceFirst(pRegStr, paramValue);
            }
        }
        return str;
    }

    /**
     * 替换参数
     *
     * @param str 带占位符${}的字符串
     * @return 替换后的字符串
     */
    public String replaceParams(String str) {
        return replaceParams(str, null);
    }

    public Map<String, Object> getAttrs() {
        return attrs;
    }

    public void setAttrs(Map<String, Object> attrs) {
        this.attrs = attrs;
    }

    public ValidationResult getResult() {
        return result;
    }

    public void setResult(ValidationResult result) {
        this.result = result;
    }
}
