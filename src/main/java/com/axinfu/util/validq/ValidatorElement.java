package com.axinfu.util.validq;

/**
 * 验证器节点
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings(value = "unused")
public class ValidatorElement {

    private Object target;
    private Validator validator;
    private int level;
    private int index;

    public ValidatorElement(Object target, Validator validator, int level, int index) {
        this.target = target;
        this.validator = validator;
        this.level = level;
        this.index = index;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
