package com.axinfu.util.validq;

import java.util.List;

/**
 * 验证回调
 *
 * @author zjn
 * @since 2022/3/23
 */
public interface ValidateCallback {

    /**
     * 验证成功
     *
     * @param validatorElements 验证器节点集合
     */
    void onSuccess(List<ValidatorElement> validatorElements);

    /**
     * 验证失败
     *
     * @param validatorElements 验证器节点集合
     * @param errors            验证过程中发生的错误
     */
    void onFail(List<ValidatorElement> validatorElements, List<ValidationError> errors);

    /**
     * 验证异常
     *
     * @param validator 验证器
     * @param e         异常
     * @param target    正在验证的对象
     * @throws Exception 异常
     */
    void onException(Validator validator, Exception e, Object target) throws Exception;
}
