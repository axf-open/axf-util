package com.axinfu.util.validq;

/**
 * 验证器
 *
 * @author zjn
 * @since 2022/3/23
 */
public interface Validator {

    /**
     * 验证优先级顺序（数字越小优先级越高，按照从小到大的顺序进行验证）
     *
     * @return 优先级
     */
    int index();

    /**
     * 启用
     *
     * @param context 上下文
     * @param target  待验证值
     * @return 当返回true时，此验证器生效
     */
    boolean accept(ValidatorContext context, Object target);

    /**
     * 执行验证
     *
     * @param context 上下文
     * @param target  待验证值
     * @return 通过验证返回true，未通过验证返回false
     */
    boolean validate(ValidatorContext context, Object target);

    /**
     * 当验证发生异常的处理
     *
     * @param e       异常
     * @param context 上下文
     * @param target  发生异常的值
     */
    void onException(Exception e, ValidatorContext context, Object target);
}
