package com.axinfu.util.validq.exception;

/**
 * 验证异常
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("all")
public class ValidQException extends RuntimeException {

    public ValidQException() {
        super();
    }

    public ValidQException(String message) {
        super(message);
    }

    public ValidQException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidQException(Throwable cause) {
        super(cause);
    }

    protected ValidQException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
