package com.axinfu.util.validq;

import com.axinfu.util.EmptyUtil;
import com.axinfu.util.validq.exception.ValidQException;

import java.util.ArrayList;
import java.util.List;

/**
 * 验证结果
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class ValidationResult {

    /**
     * 是否验证成功
     */
    private boolean isSuccess = true;

    /**
     * 是否验证失败
     */
    private boolean isFailure = false;

    /**
     * 是否发生异常
     */
    private boolean isException = false;

    /**
     * 验证错误信息
     */
    private List<ValidationError> errors = new ArrayList<>();

    /**
     * 验证时间（毫秒）
     */
    private long validTime;

    public ValidationResult() {
    }

    /**
     * 添加一个错误信息
     *
     * @param errorCode 错误代码
     * @param errorMsg  错误信息
     */
    public void addError(String errorCode, String errorMsg) {
        this.errors.add(new ValidationError(errorCode, errorMsg));
    }

    /**
     * 添加一个错误信息
     *
     * @param validationError 错误信息对象
     */
    public void addError(ValidationError validationError) {
        this.errors.add(validationError);
    }

    /**
     * 获取第一个错误信息
     *
     * @return 错误信息
     */
    public ValidationError getFirstError() {
        if (EmptyUtil.isEmpty(this.errors)) {
            throw new ValidQException("error size 0");
        }
        return this.errors.get(0);
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public boolean isFailure() {
        return isFailure;
    }

    public void setFailure(boolean isFailure) {
        this.isFailure = isFailure;
    }

    public boolean isException() {
        return isException;
    }

    public void setException(boolean isException) {
        this.isException = isException;
    }

    public List<ValidationError> getErrors() {
        return errors;
    }

    public void setErrors(List<ValidationError> errors) {
        this.errors = errors;
    }

    public long getValidTime() {
        return validTime;
    }

    public void setValidTime(long validTime) {
        this.validTime = validTime;
    }
}
