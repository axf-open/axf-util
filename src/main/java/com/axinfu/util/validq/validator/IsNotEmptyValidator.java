package com.axinfu.util.validq.validator;

import com.axinfu.util.EmptyUtil;
import com.axinfu.util.validq.BaseValidator;
import com.axinfu.util.validq.ValidatorContext;

/**
 * 验证对象不能为empty
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class IsNotEmptyValidator extends BaseValidator {

    public IsNotEmptyValidator() {
    }

    public IsNotEmptyValidator(String field, String fieldName) {
        super(field, fieldName);
    }

    public IsNotEmptyValidator(String field, String fieldName, String errorMsg) {
        super(field, fieldName, errorMsg);
    }

    @Override
    public int index() {
        return 4;
    }

    @Override
    public boolean validate(ValidatorContext context, Object target) {
        if (EmptyUtil.isNotEmpty(target)) {
            return true;
        }
        String errorMsg = getDefaultMsg("[%s]不能为空");
        return fail(context, target, errorMsg);
    }
}
