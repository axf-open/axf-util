package com.axinfu.util.validq.validator;

import com.axinfu.util.validq.BaseValidator;
import com.axinfu.util.validq.ValidatorContext;

/**
 * 验证对象不能为null
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class IsNotNullValidator extends BaseValidator {

    public IsNotNullValidator() {
    }

    public IsNotNullValidator(String field, String fieldName) {
        super(field, fieldName);
    }

    public IsNotNullValidator(String field, String fieldName, String errorMsg) {
        super(field, fieldName, errorMsg);
    }

    @Override
    public int index() {
        return 2;
    }

    @Override
    public boolean validate(ValidatorContext context, Object target) {
        if (null != target) {
            return true;
        }

        String errorMsg = getDefaultMsg("[%s]不能为空");
        return fail(context, null, errorMsg);
    }
}
