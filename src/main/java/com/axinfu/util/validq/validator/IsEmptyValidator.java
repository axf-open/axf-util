package com.axinfu.util.validq.validator;

import com.axinfu.util.EmptyUtil;
import com.axinfu.util.validq.BaseValidator;
import com.axinfu.util.validq.ValidatorContext;

/**
 * 验证对象必须为empty
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class IsEmptyValidator extends BaseValidator {

    public IsEmptyValidator() {
    }

    public IsEmptyValidator(String field, String fieldName) {
        super(field, fieldName);
    }

    public IsEmptyValidator(String field, String fieldName, String errorMsg) {
        super(field, fieldName, errorMsg);
    }

    @Override
    public int index() {
        return 3;
    }

    @Override
    public boolean validate(ValidatorContext context, Object target) {

        if (EmptyUtil.isEmpty(target)) {
            return true;
        }
        String errorMsg = getDefaultMsg("[%s]必须为空");
        return fail(context, target, errorMsg);
    }
}
