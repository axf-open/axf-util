package com.axinfu.util.validq;

/**
 * 验证错误信息
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings(value = "unused")
public class ValidationError {

    /**
     * 验证失败验证器
     */
    private Validator validator;

    /**
     * 字段（若有多级时如“filed_root.field_parent[0].field_son”）
     */
    private String field;

    /**
     * 短字段（多级时没有上级，如“field_son”）
     */
    private String shortField;

    /**
     * 字段名称（若有多级时如“跟节点.父列表[第1条].子节点”）
     */
    private String fieldName;

    /**
     * 短字段名称（多级时没有上级，如“子节点”）
     */
    private String shortFieldName;

    /**
     * 错误代码（来源于Validator）
     */
    private String errorCode;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 错误值
     */
    private Object errorValue;

    public ValidationError() {

    }

    public ValidationError(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public Validator getValidator() {
        return validator;
    }

    public ValidationError setValidator(Validator validator) {
        this.validator = validator;
        return this;
    }

    public String getField() {
        return field;
    }

    public ValidationError setField(String field) {
        this.field = field;
        return this;
    }

    public String getShortField() {
        return shortField;
    }

    public ValidationError setShortField(String shortField) {
        this.shortField = shortField;
        return this;
    }

    public String getFieldName() {
        return fieldName;
    }

    public ValidationError setFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public String getShortFieldName() {
        return shortFieldName;
    }

    public ValidationError setShortFieldName(String shortFieldName) {
        this.shortFieldName = shortFieldName;
        return this;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public ValidationError setErrorCode(String errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public ValidationError setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }

    public Object getErrorValue() {
        return errorValue;
    }

    public ValidationError setErrorValue(Object errorValue) {
        this.errorValue = errorValue;
        return this;
    }

    @Override
    public String toString() {
        return "ValidationError{" +
                "validator=" + validator +
                ", field='" + field + '\'' +
                ", shortField='" + shortField + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", shortFieldName='" + shortFieldName + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", errorValue=" + errorValue +
                '}';
    }
}
