package com.axinfu.util.validq;

import java.util.List;

/**
 * 默认验证回调
 *
 * @author zjn
 * @since 2022/3/23
 */
public class DefaultValidateCallback implements ValidateCallback {

    @Override
    public void onSuccess(List<ValidatorElement> validatorElements) {

    }

    @Override
    public void onFail(List<ValidatorElement> validatorElements, List<ValidationError> errors) {

    }

    @Override
    public void onException(Validator validator, Exception e, Object target) throws Exception {
        throw e;
    }
}
