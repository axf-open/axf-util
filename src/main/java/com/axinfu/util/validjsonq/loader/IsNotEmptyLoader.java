package com.axinfu.util.validjsonq.loader;

import com.alibaba.fastjson.JSONObject;
import com.axinfu.util.EmptyUtil;
import com.axinfu.util.validq.ValidQ;
import com.axinfu.util.validq.validator.IsNotEmptyValidator;

/**
 * loader:验证对象不能为empty
 *
 * @author zjn
 * @since 2022/3/23
 */
public class IsNotEmptyLoader extends BaseLoader {

    @Override
    public void load(ValidQ validQ, JSONObject validationConfig, Object target,
                     String attrField, String attrShortField, String attrFieldName, String attrShortFieldName,
                     Integer level, Integer index) {
        IsNotEmptyValidator validator = new IsNotEmptyValidator();

        String msg = validationConfig.getString("msg");
        if (EmptyUtil.isNotEmpty(msg)) {
            msg = "${ctx.fieldName}" + msg;
        }

        loadValidQ(validQ, validationConfig, target, attrField, attrShortField, attrFieldName, attrShortFieldName,
                level, index,
                validator, msg);
    }
}
