package com.axinfu.util.validjsonq.loader;

import com.alibaba.fastjson.JSONObject;
import com.axinfu.util.EmptyUtil;
import com.axinfu.util.validq.ValidQ;
import com.axinfu.util.validq.validator.StringLengthValidator;

/**
 * loader:验证String长度必须在min到max之间
 *
 * @author zjn
 * @since 2022/3/23
 */
public class StringLengthLoader extends BaseLoader {

    @Override
    public void load(ValidQ validQ, JSONObject validationConfig, Object target,
                     String attrField, String attrShortField, String attrFieldName, String attrShortFieldName,
                     Integer level, Integer index) {

        StringLengthValidator validator = new StringLengthValidator();

        String msg = validationConfig.getString("msg");
        Integer min = validationConfig.getInteger("min");
        Integer max = validationConfig.getInteger("max");
        if (EmptyUtil.isNotEmpty(msg)) {
            msg = "${ctx.fieldName}" + msg;
        }

        validator.setMin(min);
        validator.setMax(max);

        loadValidQ(validQ, validationConfig, target, attrField, attrShortField, attrFieldName, attrShortFieldName,
                level, index,
                validator, msg);
    }
}
