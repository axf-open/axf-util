package com.axinfu.util.validjsonq.loader;

import com.alibaba.fastjson.JSONObject;
import com.axinfu.util.EmptyUtil;
import com.axinfu.util.validq.ValidQ;
import com.axinfu.util.validq.validator.StringRegexValidator;

/**
 * loader:验证String是否符合正则匹配
 *
 * @author zjn
 * @since 2022/3/23
 */
public class StringRegexLoader extends BaseLoader {

    @Override
    public void load(ValidQ validQ, JSONObject validationConfig, Object target,
                     String attrField, String attrShortField, String attrFieldName, String attrShortFieldName,
                     Integer level, Integer index) {

        StringRegexValidator validator = new StringRegexValidator();

        String targetData = null == target ? null : String.valueOf(target);

        String msg = validationConfig.getString("msg");
        String regex = validationConfig.getString("regex");
        if (EmptyUtil.isNotEmpty(msg)) {
            msg = "${ctx.fieldName}" + msg;
        }
        validator.setRegex(regex);

        loadValidQ(validQ, validationConfig, targetData, attrField, attrShortField, attrFieldName, attrShortFieldName,
                level, index,
                validator, msg);
    }
}
