package com.axinfu.util.validjsonq.cache;

import com.alibaba.fastjson.JSONObject;
import com.axinfu.util.FileUtil;
import com.axinfu.util.PathUtil;
import com.axinfu.util.filescanner.FileScanner;

import java.io.File;
import java.util.List;

/**
 * 默认缓存加载器
 * 将制定路径下的.json文件加载到
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public class DefaultCacheLoader implements CacheLoader {

    /**
     * 加载路径
     */
    private String path;

    public DefaultCacheLoader(String path) {
        this.path = path;
    }

    public DefaultCacheLoader() {
        this.path = PathUtil.getRootClassPath();
    }

    @Override
    public void load(ValidJsonQCache cache) {
        List<File> files = FileScanner.findFiles(path, ".*\\.vjq\\.json");
        for (File file : files) {
            cache.add(file.getName().replace(".vjq.json", ""),
                    JSONObject.parseObject(FileUtil.readToString(file)));
        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
