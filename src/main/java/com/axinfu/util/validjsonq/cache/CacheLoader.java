package com.axinfu.util.validjsonq.cache;

/**
 * 缓存加载器
 *
 * @author zjn
 * @since 2022/3/23
 */
public interface CacheLoader {

    /**
     * 加载
     *
     * @param cache 缓存
     */
    void load(ValidJsonQCache cache);
}
