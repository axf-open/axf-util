package com.axinfu.util.validjsonq;

/**
 * 基于json规则验证的验证节点
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("all")
public class ValidQElement {

    /**
     * 验证器类名称
     */
    private String className;

    /**
     * 验证器名称
     */
    private String name;

    /**
     * 验证规则加载器
     */
    private ValidQLoader validatorLoader;

    public ValidQElement() {
    }

    public ValidQElement(String className, String name, ValidQLoader validatorLoader) {
        this.className = className;
        this.name = name;
        this.validatorLoader = validatorLoader;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ValidQLoader getValidatorLoader() {
        return validatorLoader;
    }

    public void setValidatorLoader(ValidQLoader validatorLoader) {
        this.validatorLoader = validatorLoader;
    }
}
