package com.axinfu.util.validjsonq.exception;

/**
 * json验证异常
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("all")
public class ValidJsonQException extends RuntimeException {

    public ValidJsonQException() {
        super();
    }

    public ValidJsonQException(String message) {
        super(message);
    }

    public ValidJsonQException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidJsonQException(Throwable cause) {
        super(cause);
    }

    protected ValidJsonQException(String message, Throwable cause, boolean enableSuppression,
                                  boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
