package com.axinfu.util.validjsonq;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.axinfu.util.validq.ValidQ;

/**
 * 基于json规则验证的装载器
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("all")
public interface ValidJsonQLoader {

    /**
     * 装载object
     *
     * @param validQ          ValidQ验证工具
     * @param group           验证分组
     * @param config          json配置
     * @param data            json待验证数据
     * @param parentField     上级字段
     * @param parentFieldName 上级字段名称
     * @param level           验证层级（数字越小优先级越高，从小到大验证）
     */
    void loadObject(ValidQ validQ, String group, JSONObject config, JSONObject data, String parentField,
                    String parentFieldName, Integer level);

    /**
     * 装载
     *
     * @param validQ          ValidQ验证工具
     * @param group           验证分组
     * @param config          json配置
     * @param data            json待验证数据
     * @param parentField     上级字段
     * @param parentFieldName 上级字段名称
     * @param level           验证层级（数字越小优先级越高，从小到大验证）
     */
    void loadList(ValidQ validQ, String group, JSONObject config, JSONArray data, String parentField,
                  String parentFieldName, Integer level);

    /**
     * 装载
     *
     * @param validQ          ValidQ验证工具
     * @param group           验证分组
     * @param config          json配置
     * @param data            json待验证数据
     * @param parentField     上级字段
     * @param parentFieldName 上级字段名称
     * @param level           验证层级（数字越小优先级越高，从小到大验证）
     */
    void loadArray(ValidQ validQ, String group, JSONObject config, JSONArray data, String parentField,
                   String parentFieldName, Integer level);
}
