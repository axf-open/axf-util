package com.axinfu.util.validjsonq;

/**
 * 数据类型
 *
 * @author zjn
 * @since 2022/3/23
 */
@SuppressWarnings("unused")
public enum DataType {

    /**
     * 字符串
     */
    STRING("string"),

    /**
     * json对象
     */
    OBJECT("object"),

    /**
     * json数组
     */
    LIST("list"),

    /**
     * jsonarray
     */
    ARRAY("array");

    private String type;

    DataType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static DataType parse(String type) {
        DataType[] items = DataType.values();
        for (DataType item : items) {
            if (item.getType().equalsIgnoreCase(type)) {
                return item;
            }
        }
        return null;
    }
}
