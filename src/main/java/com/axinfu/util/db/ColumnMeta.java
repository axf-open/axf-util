package com.axinfu.util.db;

import com.axinfu.util.enhance.JsonQ;

/**
 * ColumnMeta
 * 参考ResultSetMetaData类的定义
 *
 * @author ZJN
 * @since 2022/5/20
 */
public class ColumnMeta extends JsonQ {

    private final TableMeta tableMeta;

    /**
     * 列名
     */
    private String columnName;

    /**
     * SQL type
     */
    private int columnType;

    /**
     * SQL type 名称
     */
    private String columnTypeName;

    /**
     * 最大字符数
     */
    private int columnDisplaySize;

    /**
     * 列的指定列大小。
     * 对于数值数据，这是最大精度。
     * 对于字符数据，这是字符长度。
     * 对于日期时间数据类型，这是字符串表示的字符长度（假设小数秒组件的最大允许精度）。
     * 对于二进制数据，这是以字节为单位的长度。
     * 对于 ROWID 数据类型，这是以字节为单位的长度。
     * 对于列大小不适用的数据类型，返回 0
     */
    private int precision;

    /**
     * 列的小数点右侧的位数。
     * 对于比例不适用的数据类型，返回 0
     */
    private int scale;

    /**
     * 是否允许空值:0-否;1-是;2-未知;
     */
    private int isNullable;

    /**
     * 默认值
     */
    private String defaultValue;

    /**
     * 是否自增长
     */
    private boolean isAutoIncrement;

    /**
     * 是否区分大小写
     */
    private boolean isCaseSensitive;

    /**
     * 是否可以在search子句中使用
     */
    private boolean isSearchable;

    /**
     * 是否是货币
     */
    private boolean isCurrency;

    /**
     * 是否可以为负数
     */
    private boolean isSigned;

    /**
     * 由 SQL AS子句指定的名称
     * 如果未指定 SQL AS ，则与columnName的值相同
     */
    private String columnLabel;

    /**
     * 是否只读
     */
    private boolean isReadOnly;

    /**
     * 是否可写
     */
    private boolean isWritable;

    /**
     * 是否一定写入成功
     */
    private boolean isDefinitelyWritable;

    /**
     * 扩展：是否主键
     */
    private boolean isPrimaryKey;

    /**
     * 扩展：驼峰名称
     */
    private String name;

    /**
     * 扩展：字段备注
     */
    private String remarks;

    /**
     * 扩展：字段类型(附带字段长度与小数点)，例如：decimal(11,2)
     */
    private String type;

    /**
     * 扩展：字段对应的java类型（带包名）
     */
    private String javaTypeName;

    /**
     * 扩展：字段对应的java类型（不带包名）
     */
    private String javaType;

    /**
     * 扩展：字段对应的java类型是否为整数
     */
    private boolean isIntegerJavaType;

    /**
     * 扩展：字段对应的java类型是否为BigDecimal
     */
    private boolean isBigDecimalJavaType;

    /**
     * 扩展：字段对应的java类型是否为String
     */
    private boolean isStringJavaType;

    /**
     * 扩展：字段对应的java类型是否为Date
     */
    private boolean isDateJavaType;

    /**
     * 扩展：字段对应的java类型是否为Object
     */
    private boolean isObjectJavaType;

    /**
     * 扩展：字段对应的java类型是否为byte[]
     */
    private boolean isBytesJavaType;

    /**
     * 扩展：最小值（数值类有效）
     */
    private String min;

    /**
     * 扩展：最大值（数值类有效）
     */
    private String max;

    public ColumnMeta(TableMeta tableMeta) {
        this.tableMeta = tableMeta;
        set("tableMeta", tableMeta);
    }

    public TableMeta getTableMeta() {
        return tableMeta;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
        set("columnName", columnName);
    }

    public int getColumnType() {
        return columnType;
    }

    public void setColumnType(int columnType) {
        this.columnType = columnType;
        set("columnType", columnType);
    }

    public String getColumnTypeName() {
        return columnTypeName;
    }

    public void setColumnTypeName(String columnTypeName) {
        this.columnTypeName = columnTypeName;
        set("columnTypeName", columnTypeName);
    }

    public int getColumnDisplaySize() {
        return columnDisplaySize;
    }

    public void setColumnDisplaySize(int columnDisplaySize) {
        this.columnDisplaySize = columnDisplaySize;
        set("columnDisplaySize", columnDisplaySize);
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
        set("precision", precision);
    }

    public int getScale() {
        return scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
        set("scale", scale);
    }

    public int isNullable() {
        return isNullable;
    }

    public void setNullable(int nullable) {
        isNullable = nullable;
        set("isNullable", isNullable);
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        set("defaultValue", defaultValue);
    }

    public boolean isAutoIncrement() {
        return isAutoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        isAutoIncrement = autoIncrement;
        set("isAutoIncrement", isAutoIncrement);
    }

    public boolean isCaseSensitive() {
        return isCaseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        isCaseSensitive = caseSensitive;
        set("isCaseSensitive", isCaseSensitive);
    }

    public boolean isSearchable() {
        return isSearchable;
    }

    public void setSearchable(boolean searchable) {
        isSearchable = searchable;
        set("isSearchable", isSearchable);
    }

    public boolean isCurrency() {
        return isCurrency;
    }

    public void setCurrency(boolean currency) {
        isCurrency = currency;
        set("isCurrency", isCurrency);
    }

    public boolean isSigned() {
        return isSigned;
    }

    public void setSigned(boolean signed) {
        isSigned = signed;
        set("isSigned", isSigned);
    }

    public String getColumnLabel() {
        return columnLabel;
    }

    public void setColumnLabel(String columnLabel) {
        this.columnLabel = columnLabel;
        set("columnLabel", columnLabel);
    }

    public boolean getIsReadOnly() {
        return isReadOnly;
    }

    public void setIsReadOnly(boolean isReadOnly) {
        this.isReadOnly = isReadOnly;
        set("isReadOnly", isReadOnly);
    }

    public boolean getIsWritable() {
        return isWritable;
    }

    public void setIsWritable(boolean isWritable) {
        this.isWritable = isWritable;
        set("isWritable", isWritable);
    }

    public boolean getIsDefinitelyWritable() {
        return isDefinitelyWritable;
    }

    public void setIsDefinitelyWritable(boolean isDefinitelyWritable) {
        this.isDefinitelyWritable = isDefinitelyWritable;
        set("isDefinitelyWritable", isDefinitelyWritable);
    }

    public String isName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        set("name", name);
    }

    public boolean getIsPrimaryKey() {
        return isPrimaryKey;
    }

    public void setIsPrimaryKey(boolean isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
        set("isPrimaryKey", isPrimaryKey);
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
        set("remarks", remarks);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        set("type", type);
    }

    public String getJavaTypeName() {
        return javaTypeName;
    }

    public void setJavaTypeName(String javaTypeName) {
        this.javaTypeName = javaTypeName;
        set("javaTypeName", javaTypeName);
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
        set("javaType", javaType);
    }

    public boolean isIntegerJavaType() {
        return isIntegerJavaType;
    }

    public void setIntegerJavaType(boolean integerJavaType) {
        this.isIntegerJavaType = integerJavaType;
        set("isIntegerJavaType", integerJavaType);
    }

    public boolean isBigDecimalJavaType() {
        return isBigDecimalJavaType;
    }

    public void setBigDecimalJavaType(boolean bigDecimalJavaType) {
        this.isBigDecimalJavaType = bigDecimalJavaType;
        set("isBigDecimalJavaType", bigDecimalJavaType);
    }

    public boolean isStringJavaType() {
        return isStringJavaType;
    }

    public void setStringJavaType(boolean stringJavaType) {
        this.isStringJavaType = stringJavaType;
        set("isStringJavaType", stringJavaType);
    }

    public boolean isDateJavaType() {
        return isDateJavaType;
    }

    public void setDateJavaType(boolean dateJavaType) {
        this.isDateJavaType = dateJavaType;
        set("isDateJavaType", dateJavaType);
    }

    public boolean isObjectJavaType() {
        return isObjectJavaType;
    }

    public void setObjectJavaType(boolean objectJavaType) {
        this.isObjectJavaType = objectJavaType;
        set("isObjectJavaType", objectJavaType);
    }

    public boolean isBytesJavaType() {
        return isBytesJavaType;
    }

    public void setBytesJavaType(boolean bytesJavaType) {
        this.isBytesJavaType = bytesJavaType;
        set("isBytesJavaType", bytesJavaType);
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
        set("min", min);
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
        set("max", max);
    }
}
