package com.axinfu.util.db;

import com.axinfu.util.enhance.JsonQ;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * TableMeta
 *
 * @author ZJN
 * @since 2022/5/20
 */
public class TableMeta extends JsonQ {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表备注
     */
    private String remarks;

    /**
     * 主键
     */
    private Set<String> primaryKeys = new HashSet<>();

    /**
     * 主键，复合主键以逗号分隔
     */
    private String primaryKey;

    /**
     * 字段 meta
     */
    private List<ColumnMeta> columnMetas = new ArrayList<>();

    public TableMeta() {
        set("columnMetas", columnMetas);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
        set("tableName", tableName);
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
        set("remarks", remarks);
    }

    public Set<String> getPrimaryKeys() {
        return primaryKeys;
    }

    public void setPrimaryKeys(Set<String> primaryKeys) {
        this.primaryKeys = primaryKeys;
        set("primaryKeys", primaryKeys);
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
        set("primaryKey", primaryKey);
    }

    public List<ColumnMeta> getColumnMetas() {
        return columnMetas;
    }

    public void setColumnMetas(List<ColumnMeta> columnMetas) {
        this.columnMetas = columnMetas;
        set("columnMetas", columnMetas);
    }
}
