package com.axinfu.util.db;

import com.axinfu.util.db.dialect.DefaultDialect;

import javax.sql.DataSource;
import java.util.List;
import java.util.function.Predicate;

/**
 * DbGenUtil
 *
 * @author ZJN
 * @since 2022/5/20
 */
public final class DbUtil {

    /**
     * 方言
     */
    public static final DefaultDialect DEFAULT_DIALECT = new DefaultDialect();

    /**
     * 是否跳过表
     */
    public static final Predicate<String> DEFAULT_IS_SKIP_TABLE = (tableName) -> false;

    /**
     * "TABLE","VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY","LOCAL TEMPORARY", "ALIAS", "SYNONYM"
     */
    public static final String[] DEFAULT_TYPES = new String[]{"TABLE"};

    /**
     * sql类型与java类型转换规则
     */
    public static final HandleJavaType DEFAULT_HANDLE_JAVA_TYPE = new HandleJavaType();

    private DbUtil() {
    }

    public static List<TableMeta> genMetaData(DataSource dataSource) {
        return new MetaBuilder(dataSource).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param dialect        dialect
     * @param isSkipTable    isSkipTable
     * @param types          types
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect,
                                              Predicate<String> isSkipTable, String[] types,
                                              HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, dialect, isSkipTable, types, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource  dataSource
     * @param dialect     dialect
     * @param isSkipTable isSkipTable
     * @param types       types
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect,
                                              Predicate<String> isSkipTable, String[] types) {

        return new MetaBuilder(dataSource, dialect, isSkipTable, types).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param dialect        dialect
     * @param isSkipTable    isSkipTable
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect,
                                              Predicate<String> isSkipTable,
                                              HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, dialect, isSkipTable, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param dialect        dialect
     * @param types          types
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect, String[] types,
                                              HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, dialect, types, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param isSkipTable    isSkipTable
     * @param types          types
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, Predicate<String> isSkipTable, String[] types,
                                              HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, isSkipTable, types, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource  dataSource
     * @param dialect     dialect
     * @param isSkipTable isSkipTable
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect,
                                              Predicate<String> isSkipTable) {

        return new MetaBuilder(dataSource, dialect, isSkipTable).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param dialect        dialect
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect,
                                              HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, dialect, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param types          types
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, String[] types,
                                              HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, types, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource dataSource
     * @param dialect    dialect
     * @param types      types
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect, String[] types) {

        return new MetaBuilder(dataSource, dialect, types).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource  dataSource
     * @param isSkipTable isSkipTable
     * @param types       types
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, Predicate<String> isSkipTable, String[] types) {

        return new MetaBuilder(dataSource, isSkipTable, types).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param isSkipTable    isSkipTable
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, Predicate<String> isSkipTable,
                                              HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, isSkipTable, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource     dataSource
     * @param handleJavaType handleJavaType
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, HandleJavaType handleJavaType) {

        return new MetaBuilder(dataSource, handleJavaType).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource dataSource
     * @param types      types
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, String[] types) {

        return new MetaBuilder(dataSource, types).build();
    }

    /**
     * MetaBuilder
     *
     * @param dataSource  dataSource
     * @param isSkipTable isSkipTable
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, Predicate<String> isSkipTable) {
        return new MetaBuilder(dataSource, isSkipTable).build();
    }


    /**
     * MetaBuilder
     *
     * @param dataSource dataSource
     * @param dialect    dialect
     * @return List
     */
    public static List<TableMeta> genMetaData(DataSource dataSource, DefaultDialect dialect) {
        return new MetaBuilder(dataSource, dialect).build();
    }
}
