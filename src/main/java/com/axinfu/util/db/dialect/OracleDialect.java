package com.axinfu.util.db.dialect;

/**
 * OracleDialect
 *
 * @author ZJN
 * @since 2022/5/20
 */
public class OracleDialect extends DefaultDialect {

    public String forTableBuilderDoBuild(String tableName) {
        return "select * from " + tableName + " where rownum < 1";
    }
}
