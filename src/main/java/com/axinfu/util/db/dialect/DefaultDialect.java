package com.axinfu.util.db.dialect;

/**
 * DefaultDialect
 *
 * @author ZJN
 * @since 2022/5/20
 */
public class DefaultDialect {

    /**
     * Methods for common
     *
     * @param tableName tableName
     * @return String
     */
    public String forTableBuilderDoBuild(String tableName) {
        return "select * from `" + tableName + "` where 1 = 2";
    }
}
