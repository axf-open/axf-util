package com.axinfu.util;

/**
 * Constant
 *
 * @author zjn
 * @since 2022/3/23
 */
public class Constant {
    public static final char CHAR_0 = '0';
    public static final char CHAR_LOWER_A = 'a';
    public static final char CHAR_UPPER_A = 'A';
    public static final String STR_SLASH = "/";
}
