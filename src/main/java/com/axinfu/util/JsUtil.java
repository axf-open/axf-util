package com.axinfu.util;

import com.axinfu.util.enhance.JsonQ;

import java.util.Map;

/**
 * 处理js
 *
 * @author ZJN
 * @since 2022/5/13
 */
public class JsUtil {

    private JsUtil() {
    }

    public static String escapeString(String s) {
        if (EmptyUtil.isEmpty(s)) {
            return s;
        }
        return s.replace("\\", "\\\\");
    }

    public static void escapeJsonQ(JsonQ jsonQ) {
        for (Map.Entry<String, Object> entry : jsonQ.entrySet()) {
            if (entry.getValue() instanceof String) {
                jsonQ.set(entry.getKey(), escapeString(String.valueOf(entry.getValue())));
            }
        }
    }

    public static void escapeObjectMap(Map<String, Object> map) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() instanceof String) {
                map.put(entry.getKey(), escapeString(String.valueOf(entry.getValue())));
            }
        }
    }

    public static void escapeStringMap(Map<String, String> map) {
        map.replaceAll((k, v) -> escapeString(String.valueOf(v)));
    }
}
