package com.axinfu.util.enhance;

/**
 * FinalObject
 *
 * @param <T> T
 * @author zjn
 * @since 2022/3/21
 */
public class FinalObject<T> {

    private T value;

    public FinalObject() {
    }

    public FinalObject(T value) {
        this.value = value;
    }

    public static <T> FinalObject<T> create() {
        return new FinalObject<T>();
    }

    public static <T> FinalObject<T> create(T value) {
        return new FinalObject<T>(value);
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
