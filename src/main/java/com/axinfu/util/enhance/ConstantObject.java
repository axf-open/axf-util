package com.axinfu.util.enhance;

/**
 * ConstantObject
 *
 * @param <M> M
 * @author zjn
 * @since 2022/4/18
 */
public class ConstantObject<M> {

    public M asNull() {
        return null;
    }
}
