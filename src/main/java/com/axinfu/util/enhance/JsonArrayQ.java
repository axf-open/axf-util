package com.axinfu.util.enhance;

import com.alibaba.fastjson.JSONArray;
import com.axinfu.util.EmptyUtil;

/**
 * 快速构建JsonArray对象
 * ps:取名为JsonArrayQ意义在于，Q位于键盘左上角
 * ，由左手小指负责，小指平时使用较少
 * ，这样可以多锻炼，利于开发右脑
 *
 * @author zjn
 * @since 2022/3/23
 */
public class JsonArrayQ extends JSONArray {

    public JsonArrayQ() {
    }

    public static JsonArrayQ by(JsonQ jsonQ) {
        return new JsonArrayQ().ad(jsonQ);
    }

    public static JsonArrayQ byo(Object obj) {
        return new JsonArrayQ().ado(obj);
    }

    public static JsonArrayQ create() {
        return new JsonArrayQ();
    }

    public JsonArrayQ ad(JsonQ jsonQ) {
        if (EmptyUtil.isNotEmpty(jsonQ)) {
            super.add(jsonQ);
        }
        return this;
    }

    public JsonArrayQ ad(JSONArray jsonArray) {
        super.addAll(jsonArray);
        return this;
    }

    public JsonArrayQ ado(Object obj) {
        if (EmptyUtil.isNotEmpty(obj)) {
            super.add(obj);
        }
        return this;
    }

    public JsonQ getJsonQ(int index) {
        return JsonQ.create().set(this.getJSONObject(index));
    }
}
