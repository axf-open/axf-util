/**
 * Created by Administrator on 2017/1/23.
 */
$(function () {
    document.onkeydown = function (event) {

        var e = event || window.event || arguments.callee.caller.arguments[0];

        if (e && e.keyCode == 13) { // enter 键  //要做的事情
            sub();
        }

    };
})

function sub() {
    var login_id = $("#login_id").val();
    var login_pwd = $("#login_pwd").val();

    loading.show();
    $.ajax({
        url: "btgset/login.json",
        type: "post",
        data: {
            login_id: login_id,
            login_pwd: login_pwd
        },
        dataType: "json",
        error: onajaxerror,
        success: function (re) {
            if (isSuccess(re)) {
                setCookie(re.key, re[re.key])
                changeUrl("index.html");
            } else {
                onAjaxRequestFailure(re);
            }
        }
    });
}