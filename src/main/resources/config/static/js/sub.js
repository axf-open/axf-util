$(function () {
})
var paramData;
function setData(data) {
    paramData = data;
    init(data);
}
function init(data) {
    loading.show();
    $.ajax({
        url: "btgset/sub.json",
        type: "post",
        data: {
            module: data.key_id,
            bak: data.bak
        },
        dataType: "json",
        error: onajaxerror,
        success: function (result) {
            loading.hide();
            if (isSuccess(result)) {
                $("#key_id").val(result['key_id']);
                createEl(result['re'])
                bak(data.bak)
            } else {
                onAjaxRequestFailure(result);
            }
        }
    });
}
function bak(con) {
    if (con != undefined && con) {
        $("input[name='item_value']").attr("readOnly", "readOnly")
    }
}
function createEl(data) {
    data.forEach(function (v, i) {
        var fieldset = $("#hide_div").find("fieldset").clone();
        fieldset.find("legend").html(v['group_name'])

        var ch = v['data'];
        ch.forEach(function (a, i) {
            var row = $("#hide_div").find(".row").clone();
            var head_div = row.find("div[name=head_div]");
            var inp = row.find("div[name=value_div] input");
            var str = "<span name='item_code' style='margin-left: 5px;color: orange'>" + a['item_code'] + "</span>" +
                "<br/><br/>【" + a['item_name'] + "】";
            if (a['data_type'] != "") {
                str += "【" + a['data_type'] + "】"
            } else {
                str += "【String】"
            }
            if (a['remark'] != "" && a['remark'] != undefined)
                str += "<br/>说明：" + a['remark']
            head_div.html(str)
            inp.val(a['item_value'])
            fieldset.append(row);
        })

        $(".container").append(fieldset);
    })
}
function save() {

    if (paramData.bak != undefined && paramData.bak) {
        closeWindow();
    } else {
        var rows = $(".container .row");
        var arr = [];
        rows.each(function () {
            var data = {};
            var code = $(this).find("span[name=item_code]").text();
            var value = $(this).find("input[name=item_value]").val();
            data.item_code = code;
            data.item_value = value;
            arr.push(data);
        })
        loading.show();
        $.ajax({
            url: "btgset/save.json",
            type: "post",
            data: {
                data: window.JSON.stringify(arr),
                key_id: $("#key_id").val()
            },
            dataType: "json",
            error: onajaxerror,
            success: function (result) {
                loading.hide();
                if (isSuccess(result)) {
                    closeWindow();
                } else {
                    onAjaxRequestFailure(result);
                }
            }
        });
    }

}
function closeWindow(data) {
    paramData['closeWindow'].call(this, data);
}