var tab = $("#tab");
$(function () {
    query();
})

function query() {
    loading.show();
    $.ajax({
        url: "btgset/query.json",
        type: "post",
        data: {},
        dataType: "json",
        error: onajaxerror,
        success: function (result) {
            loading.hide();
            if (isSuccess(result)) {
                tab.bootstrapTable("load", result.data)
                showCustom(result.custom);
            } else {
                onAjaxRequestFailure(result);
            }
        }
    });
}

var showCustom = function (arr) {
    var p = $("#cus_div");
    if (arr) {
        $.each(arr, function (i, v) {
            var div = $("#tpl div:first").clone();
            var btn = $(div).find("button");
            btn.html(v.title)
            btn.attr("index", v.index);
            $(div).find("input").val(v.desc)
            p.append(div);

            btn.click(function () {
                doExcute(v.index);
            })
        })

    }
}
var doExcute = function (idx) {
    confirmMsg("请谨慎使用此功能，是否继续？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgset/doExcute.json",
                type: "post",
                data: {
                    index: idx
                },
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {
                        alertMsg(result.msg)
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });


}

function initEvn() {
    confirmMsg("此操作会删除目前的所有配置信息，并加载默认的配置内容是否继续？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgset/initEvn.json",
                type: "post",
                data: {},
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {
                        alertMsg(result.msg)
                        query();
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });

}

function stateFormat(value, row) {
    var re = "";
    switch (value) {
        case "0":
            re = "<span style='color:green'>未使用</span>";
            break;
        case "1":
            re = "<span style='color:red'>正在使用</span>";
            break;
    }
    return re;
}

function copyDef() {
    var set_code = $("#set_code").val()
    var set_name = $("#set_name").val()
    var remark = $("#remark").val()

    if (set_code == "") {
        alertMsg("请输入配置代码！");
        return;
    }
    if (set_name == "") {
        alertMsg("请输入配置名称！");
        return;
    }
    loading.show();
    $.ajax({
        url: "btgset/copyDef.json",
        type: "post",
        data: {
            set_code: set_code,
            set_name: set_name,
            remark: remark
        },
        dataType: "json",
        error: onajaxerror,
        success: function (result) {
            loading.hide();
            if (isSuccess(result)) {
                $('#myModal').modal('hide')
                query();
            } else {
                onAjaxRequestFailure(result);
            }
        }
    });
}

function sub(row) {
    var key_id = "";
    if (row) {
        var rows = tab.bootstrapTable("getSelections");
        if (rows.length != 1) {
            alertMsg("请选择一行数据再进行操作", {
                icon: 5
            })
            return;
        }
        key_id = rows[0]['key_id'];

    }
    btg.open({
        title: "查询当前配置",
        max: true,
        data: {
            key_id: key_id
        },
        btn: ['保存', '关闭'],
        doSubmit: "save",
        url: "sub.html",
        isTop: true,
        ondestroy: function (re) {
        }
    });
}

function applySet() {
    var rows = tab.bootstrapTable("getSelections");
    if (rows.length != 1) {
        alertMsg("请选择一行数据再进行操作", {
            icon: 5
        })
        return;
    }
    var key_id = rows[0]['key_id']
    confirmMsg("是否应用当前选中项【" + rows[0]['set_name'] + "】的配置信息？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgset/apply.json",
                type: "post",
                data: {
                    key_id: key_id
                },
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {
                        alertMsg(result.msg)
                        query();
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });
}

function del() {
    var rows = tab.bootstrapTable("getSelections");
    if (rows.length != 1) {
        alertMsg("请选择一行数据再进行操作", {
            icon: 5
        })
        return;
    }
    if (rows[0]['state'] == 1) {
        alertMsg("正在使用的配置信息不可删除！", {
            icon: 5
        })
        return;
    }
    var key_id = rows[0]['key_id']
    confirmMsg("是否删除当前选中项【" + rows[0]['set_name'] + "】的配置信息？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgset/del.json",
                type: "post",
                data: {
                    key_id: key_id
                },
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {
                        alertMsg(result.msg)
                        query();
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });
}

function expSet() {
    var rows = tab.bootstrapTable("getSelections");
    if (rows.length != 1) {
        alertMsg("请选择一行数据再进行操作", {
            icon: 5
        })
        return;
    }
    var key_id = rows[0]['key_id']
    var name = rows[0]['set_name']
    confirmMsg("是否导出【" + rows[0]['set_name'] + "】的配置信息？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgset/expSet.json",
                type: "post",
                data: {
                    module: key_id,
                    name: name
                },
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {

                        var form = $("#download");
                        if (form.length == 0)
                            form.remove();
                        form = $("<form id='download'>");//定义一个form表单
                        form.attr("style", "display:none");
                        form.attr("target", "");
                        form.attr("method", "post");
                        form.attr("action", "btgset/download.json");
                        var input1 = $("<input>");
                        input1.attr("type", "hidden");
                        input1.attr("name", "xml_name");
                        input1.attr("value", result['xml_name']);
                        $("body").append(form);//将表单放置在web中
                        form.append(input1);
                        form.submit();//表单提交
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });

}

function bakSet() {
    var rows = tab.bootstrapTable("getSelections");
    if (rows.length != 1) {
        alertMsg("请选择一行数据再进行操作", {
            icon: 5
        })
        return;
    }
    var key_id = rows[0]['key_id']

    confirmMsg("之前备份信息将被覆盖，是否继续？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgset/bakSet.json",
                type: "post",
                data: {
                    module: key_id
                },
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {
                        alertMsg(result.msg)
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });

}

function viewBak() {

    loading.show();
    $.ajax({
        url: "btgset/getBakTime.json",
        type: "post",
        data: {},
        dataType: "json",
        error: onajaxerror,
        success: function (result) {
            loading.hide()
            if (isSuccess(result)) {
                btg.open({
                    title: "备份配置查询，备份时间：" + result['i_time'],
                    max: true,
                    data: {
                        bak: true
                    },
                    btn: ['关闭'],
                    doSubmit: "save",
                    url: "sub.html",
                    isTop: true,
                    ondestroy: function (re) {
                    }
                });
            } else {
                onAjaxRequestFailure(result);
            }
        }
    });

}

function recoverSet() {
    var rows = tab.bootstrapTable("getSelections");
    if (rows.length != 1) {
        alertMsg("请选择一行数据再进行操作", {
            icon: 5
        })
        return;
    }
    var key_id = rows[0]['key_id']

    confirmMsg("当前选中项【" + rows[0]['set_name'] + "】的配置信息将被恢复，是否继续？", {
        yes: function () {
            loading.show();
            $.ajax({
                url: "btgset/recoverSet.json",
                type: "post",
                data: {
                    module: key_id
                },
                dataType: "json",
                error: onajaxerror,
                success: function (result) {
                    loading.hide()
                    if (isSuccess(result)) {
                        alertMsg(result.msg)
                    } else {
                        onAjaxRequestFailure(result);
                    }
                }
            });
        }
    });

}