/**
 * Created by JLT on 2016/10/27.
 */


/**
 * 初始化 bootstrap table
 * 不用写那么多 options,此方法没有列出的 options,用 extend去扩展
 * @param tableId
 * @param options
 * @returns {jQuery}
 */
function initBootstrapTable(tableId, options) {

    var height = options.height == undefined ? undefined : options.height;
    var url = options.url;
    var method = options.method == undefined ? 'POST' : options.method;
    var contentType = options.contentType == undefined ? "application/x-www-form-urlencoded" : options.contentType;
    var cache = options.cache == undefined ? false : options.cache; // 不缓存
    var striped = options.striped == undefined ? true : options.striped; // 隔行加亮
    var pagination = options.pagination == undefined ? true : options.pagination; // 开启分页功能
    var pageSize = options.pageSize == undefined ? 5 : options.pageSize; // 设置默认分页为 5
    var search = options.search == undefined ? false : options.search; // 开启搜索功能
    var showColumns = options.showColumns == undefined ? true : options.showColumns; // 开启自定义列显示功能
    var showRefresh = options.showRefresh == undefined ? true : options.showRefresh; // 开启刷新功能
    var minimumCountColumns = options.minimumCountColumns == undefined ? 1 : options.minimumCountColumns; // 设置最少显示列个数
    var clickToSelect = options.clickToSelect == undefined ? true : options.clickToSelect; // 单击行即可以选中
    var sortName = options.sortName == undefined ? 'id' : options.sortName; // 设置默认排序为 name
    var sortOrder = options.sortOrder == undefined ? 'desc' : options.sortOrder; // 设置排序为反序 desc
    var smartDisplay = options.smartDisplay == undefined ? true : options.smartDisplay; // 智能显示 pagination 和 cardview 等
    var dataField = options.dataField == undefined ? 'rows' : options.dataField;
    var showToggle = options.showToggle == undefined ? true : options.showToggle;
    var sidePagination = options.sidePagination == undefined ? 'server' : options.sidePagination;
    var pageList = options.pageList == undefined ? [5, 10, 15, 20, 'ALL'] : options.pageList;
    var queryParams = options.queryParams == undefined ? function (params) {
        return params;
    } : options.queryParams;
    var columns = options.columns;
    var undefinedText = options.undefinedText == undefined ? "-" : options.undefinedText;
    var toolbar = options.toolbar == undefined ? "" : options.toolbar;
    var singleSelect = options.singleSelect == undefined ? false : options.singleSelect;
    var rowStyle = options.rowStyle == undefined ? function (row, index) {
        return {
            css: {"font-size": "12px", "line-height": "15px", "white-space": "nowrap"}
        };
    } : options.rowStyle;

    // extend 扩展字段,用于options中没有列出的属性
    var extend = options.extend == undefined ? {} : options.extend;

    var option = {
        height: height,
        url: url,
        method: method,
        contentType: contentType,
        undefinedText: undefinedText,
        cache: cache, // 不缓存
        striped: striped, // 隔行加亮
        pagination: pagination, // 开启分页功能
        pageSize: pageSize, // 设置默认分页为 5
        pageList: pageList,
        search: search, // 开启搜索功能
        showColumns: showColumns, // 开启自定义列显示功能
        showRefresh: showRefresh, // 开启刷新功能
        minimumCountColumns: minimumCountColumns, // 设置最少显示列个数
        clickToSelect: clickToSelect, // 单击行即可以选中
        sortName: sortName, // 设置默认排序为 name
        sortOrder: sortOrder, // 设置排序为反序 desc
        smartDisplay: smartDisplay, // 智能显示 pagination 和 cardview 等
        dataField: dataField,
        showToggle: showToggle,
        sidePagination: sidePagination,
        pageList: pageList,
        queryParams: queryParams,
        columns: columns,
        toolbar: toolbar,
        singleSelect: singleSelect,
        rowStyle: rowStyle
    };

    if (extend) {
        for (var key in  extend) {
            option[key] = extend[key];
        }
    }

    $(tableId).bootstrapTable(option);

    return $(tableId).bootstrapTable();
}
/**
 * 获取 bootstrap table 行号
 * @param bootstrapTable
 * @param index
 * @returns {*}
 */
function getRowNumber(bootstrapTable, index) {
    var pageSize = bootstrapTable.bootstrapTable('getOptions').pageSize;
    var pageNumber = bootstrapTable.bootstrapTable('getOptions').pageNumber;
    return pageSize * (pageNumber - 1) + index + 1;
}
var initParams;
var searchParam = null;
function queryParams(params) {

    if (!initParams) {
        initParams = params;
    }
    if (searchParam != null) {
        for (var s in searchParam) {
            params[s] = searchParam[s];
        }
    } else {
        for (var s in initParams) {
            if (s != "limit" && s != "offset")
                params[s] = initParams[s];
        }
    }
    return params;
}
function refreshTableInfoByData(tableId, data) {
    if (data)
        searchParam = data;
    $("#" + tableId).bootstrapTable("refreshOptions", {
        pageNumber: 1,
        queryParams: function (params) {
            return queryParams(params);
        }
    });
}

function refreshTableInfoByForm(tableId, searchFormId) {
    var data = serializeForm($("#" + searchFormId));
   
    refreshTableInfoByData(tableId, data)
}
function resetSearch(tableId, searchFormId) {
    $("#" + searchFormId)[0].reset();
    searchParam = null;
    refreshTableInfoByData(tableId, null)
}


/**
 * 添加额外参数
 * @param params
 * @param item
 * @returns {*}
 */
function addParams(params, item) {
    for (var key in item) {
        params[key] = item[key];
    }
    return params;
}

function getBootstrapTableQueryParam(formId, params) {
    var param = addParams(params, serializeForm($(formId)));
    param.pageSize = params.limit;
    param.currentPage = params.offset / params.limit + 1;
    return param;
}


//让 bootstrap table 使用简体中文
$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['zh-CN']);
