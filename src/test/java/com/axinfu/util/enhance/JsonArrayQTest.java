package com.axinfu.util.enhance;

import com.alibaba.fastjson.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * JsonQArray Tester.
 */
@SuppressWarnings("all")
public class JsonArrayQTest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: by(JsonQ jsonQ)
     */
    @Test
    public void testBy() throws Exception {
        JsonArrayQ jsonArrayQ = JsonArrayQ.by(JsonQ.by("k1", "v1"));
        System.out.println(jsonArrayQ);
    }

    /**
     * Method: byo(Object obj)
     */
    @Test
    public void testByo() throws Exception {
        JsonArrayQ jsonArrayQ = JsonArrayQ.byo(new String("111"));
        System.out.println(jsonArrayQ);
    }

    /**
     * Method: create()
     */
    @Test
    public void testCreate() throws Exception {
        JsonArrayQ jsonArrayQ = JsonArrayQ.create();
        jsonArrayQ.add("111");
        System.out.println(jsonArrayQ);
    }

    /**
     * Method: ad(JsonQ jsonQ)
     */
    @Test
    public void testAdJsonQ() throws Exception {
        JsonArrayQ jsonArrayQ = JsonArrayQ.create().ad(JsonQ.by("k1", "v1"));
        System.out.println(jsonArrayQ);
    }

    /**
     * Method: ad(JSONArray jsonArray)
     */
    @Test
    public void testAdJsonArray() throws Exception {
        JSONArray jsonArray = JsonArrayQ.parseArray("[{\"k1\":\"v1\"}]");
        JsonArrayQ jsonArrayQ = JsonArrayQ.create().ad(jsonArray);
        System.out.println(jsonArrayQ);
    }

    /**
     * Method: ado(Object obj)
     */
    @Test
    public void testAdo() throws Exception {
        JsonArrayQ jsonArrayQ = JsonArrayQ.create();
        jsonArrayQ.ado("111");
        System.out.println(jsonArrayQ);
    }

    /**
     * Method: getJsonQ(int index)
     */
    @Test
    public void testGetJsonQ() throws Exception {
        JsonArrayQ jsonArrayQ = JsonArrayQ.create().ad(JsonQ.by("k1", "v1"));
        JsonQ jsonQ = jsonArrayQ.getJsonQ(0);
        System.out.println(jsonQ);
    }
}
