package com.axinfu.util.enhance;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * JsonQ Tester.
 */
@SuppressWarnings("all")
public class JsonQTest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: create()
     */
    @Test
    public void testCreate() throws Exception {
        JsonQ jsonQ = JsonQ.create().set("k1", "v1");
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: create(Map<String, Object> map)
     */
    @Test
    public void testCreateMap() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("k1", "v1");
        JsonQ jsonQ = JsonQ.create(map);
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: create(String json)
     */
    @Test
    public void testCreateJson() throws Exception {
        JsonQ jsonQ = JsonQ.create(JsonQ.create().set("k1", "v1").toJSONString());
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: create(boolean ordered)
     */
    @Test
    public void testCreateOrdered() throws Exception {
        JsonQ jsonQ = JsonQ.create(true).set("k1", "v1");
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: create(int initialCapacity)
     */
    @Test
    public void testCreateInitialCapacity() throws Exception {
        JsonQ jsonQ = JsonQ.create(1).set("k1", "v1");
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: create(int initialCapacity, boolean ordered)
     */
    @Test
    public void testCreateForInitialCapacityOrdered() throws Exception {
        JsonQ jsonQ = JsonQ.create(1, true).set("k1", "v1");
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: by(String key, Object value)
     */
    @Test
    public void testByForKeyValue() throws Exception {
        JsonQ jsonQ = JsonQ.by("k1", "v1");
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: by(Map<String, Object> map)
     */
    @Test
    public void testByMap() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("k1", "v1");
        JsonQ jsonQ = JsonQ.by(map);
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: set(String key, Object value)
     */
    @Test
    public void testSetForKeyValue() throws Exception {
        JsonQ jsonQ = JsonQ.create();
        jsonQ.set("k1", "v1");
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: set(Map<String, Object> map)
     */
    @Test
    public void testSetMap() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("k1", "v1");

        JsonQ jsonQ = JsonQ.create();
        jsonQ.set(map);
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: set(String json)
     */
    @Test
    public void testSetJson() throws Exception {
        JsonQ jsonQ = JsonQ.create();
        jsonQ.set(JsonQ.create().set("k1", "v1").toJSONString());
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: delete(String key)
     */
    @Test
    public void testDelete() throws Exception {
        JsonQ jsonQ = JsonQ.by("k1", "v1").delete("k1");
        System.out.println(jsonQ.toJSONString());
    }

    /**
     * Method: getAs(String key)
     */
    @Test
    public void testGetAs() throws Exception {
        JsonQ jsonQ = JsonQ.by("k1", "v1");
        String s = jsonQ.getAs("k1");
        System.out.println(s);
    }

    /**
     * Method: toMap()
     */
    @Test
    public void testToMap() throws Exception {
        JsonQ jsonQ = JsonQ.by("k1", "v1");
        Map<String, Object> map = jsonQ.toMap();
        System.out.println(map);
    }

    /**
     * Method: toMapString()
     */
    @Test
    public void testToMapString() throws Exception {
        JsonQ jsonQ = JsonQ.by("k1", "v1");
        Map<String, String> map = jsonQ.toMapString();
        System.out.println(map);
    }
}
