package com.axinfu.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * StringUtil Tester.
 */
@SuppressWarnings("all")
public class StringUtilTest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: format2length(String str, int fillLength, String fillChar, int fillIndex, boolean isTrancate)
     */
    @Test
    public void testFormat2lengthForStrFillLengthFillCharFillIndexIsTrancate() throws Exception {
        int len = 50;
        System.out.println(StringUtil.format2length("", len, "-", 0, true));
        System.out.println(StringUtil.format2length("配置列表", len / 2, "-", 0, true));
        System.out.println(StringUtil.format2length("配置列表配置列表配置列表配置列表配置列表配置列表配置列表", len / 2, "-", 0, true));
        System.out.println(StringUtil.format2length("", len, "-", 0, true));
    }

    /**
     * Method: format2length(String str, int fillLength, String fillChar, int fillIndex)
     */
    @Test
    public void testFormat2lengthForStrFillLengthFillCharFillIndex() throws Exception {
        int len = 50;
        System.out.println(StringUtil.format2length("", len, "-", 0));
        System.out.println(StringUtil.format2length("配置列表", len / 2, "-", 0));
        System.out.println(StringUtil.format2length("配置列表配置列表配置列表配置列表配置列表配置列表配置列表", len / 2, "-", 0));
        System.out.println(StringUtil.format2length("", len, "-", 0));
    }

    /**
     * Method: format2length(String str, int fillLength, String fillChar)
     */
    @Test
    public void testFormat2lengthForStrFillLengthFillChar() throws Exception {
        int len = 50;
        System.out.println(StringUtil.format2length("", len, "-"));
        System.out.println(StringUtil.format2length("配置列表", len / 2, "-"));
        System.out.println(StringUtil.format2length("配置列表配置列表配置列表配置列表配置列表配置列表配置列表", len / 2, "-"));
        System.out.println(StringUtil.format2length("", len, "-"));
    }

    /**
     * Method: format2length(String str, int fillLength)
     */
    @Test
    public void testFormat2lengthForStrFillLength() throws Exception {
        int len = 50;
        System.out.println(StringUtil.format2length("", len));
        System.out.println(StringUtil.format2length("配置列表", len / 2));
        System.out.println(StringUtil.format2length("配置列表配置列表配置列表配置列表配置列表配置列表配置列表", len / 2));
        System.out.println(StringUtil.format2length("", len));
    }

    /**
     * Method: addLastSuffix(String uri, String suffix)
     */
    @Test
    public void testAddLastSuffixForUriSuffix() throws Exception {
        String uriHttp = "http://www.baidu.com";
        System.out.println(StringUtil.addLastSuffix(uriHttp, "/"));
        String uriHttp2 = "http://www.baidu.com/";
        System.out.println(StringUtil.addLastSuffix(uriHttp2, "/"));

        String uriFile = "d:/aa/bb";
        System.out.println(StringUtil.addLastSuffix(uriFile, "/"));
        String uriFile2 = "d:/aa/bb/";
        System.out.println(StringUtil.addLastSuffix(uriFile2, "/"));
    }

    /**
     * Method: addLastSuffix(String uri)
     */
    @Test
    public void testAddLastSuffixUri() throws Exception {
        String uriHttp = "http://www.baidu.com";
        System.out.println(StringUtil.addLastSuffix(uriHttp));
        String uriHttp2 = "http://www.baidu.com/";
        System.out.println(StringUtil.addLastSuffix(uriHttp2));

        String uriFile = "d:/aa/bb";
        System.out.println(StringUtil.addLastSuffix(uriFile));
        String uriFile2 = "d:/aa/bb/";
        System.out.println(StringUtil.addLastSuffix(uriFile2));
    }

    /**
     * Method: lineToHump(String str, boolean isFirstUpper)
     */
    @Test
    public void testLineToHumpForStrIsFirstUpper() throws Exception {
        System.out.println(StringUtil.lineToHump("sys_user", true));
    }

    /**
     * Method: lineToHump(String str)
     */
    @Test
    public void testLineToHumpStr() throws Exception {
        System.out.println(StringUtil.lineToHump("sys_user"));
    }

    /**
     * Method: humpToLine(String str)
     */
    @Test
    public void testHumpToLine2() throws Exception {
        System.out.println(StringUtil.humpToLine("sysUser"));
    }

    @Test
    public void joinObject() {
        Object[] array = {"a", "b"};
        System.out.println(StringUtil.joinObject(array, ","));
    }

    @Test
    public void testJoinObject() {
        Object[] array = {"a", "b"};
        System.out.println(StringUtil.joinObject(array));
    }

    @Test
    public void testJoinObject1() {
        List<Object> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        System.out.println(StringUtil.joinObject(list, ","));
    }

    @Test
    public void testJoinObject2() {
        List<Object> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        System.out.println(StringUtil.joinObject(list));
    }

    @Test
    public void Join() {
        String[] array = {"a", "b"};
        System.out.println(StringUtil.join(array, ","));
    }

    @Test
    public void testJoin() {
        String[] array = {"a", "b"};
        System.out.println(StringUtil.join(array));
    }

    @Test
    public void testJoin1() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        System.out.println(StringUtil.join(list, ","));
    }

    @Test
    public void testJoin2() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        System.out.println(StringUtil.join(list));
    }
}
