package com.axinfu.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * FileUtil Tester.
 */
@SuppressWarnings("all")
public class FileUtilTest {

    private String basePath;
    private String filePath;
    private String filePathCopy;

    @Before
    public void before() throws Exception {
        basePath = System.getProperty("user.dir");
        filePath = basePath + "/a/d/f.txt";
        filePathCopy = basePath + "/a/d/f_copy.txt";

        FileUtil.write(filePath, "Hellow 世界！");
    }

    @After
    public void after() throws Exception {
        clean();
    }

    /**
     * 善后，删除创建的那些垃圾文件或文件夹
     */
    private void clean() {
        FileUtil.rmFile(basePath + "/a", true);
        FileUtil.rmFile(basePath + "/b", true);
        FileUtil.rmFile(basePath + "/c", true);
    }

    /**
     * Method: isExists(File file)
     */
    @Test
    public void testExistsFile() throws Exception {
        File file = new File(filePath);
        System.out.println(FileUtil.isExists(file));
    }

    /**
     * Method: isExists(String filePath)
     */
    @Test
    public void testExistsFilePath() throws Exception {
        System.out.println(FileUtil.isExists(filePath));
    }

    /**
     * Method: isNotExists(File file)
     */
    @Test
    public void testNotExistsFile() throws Exception {
        File file = new File(filePath);
        System.out.println(FileUtil.isNotExists(file));
    }

    /**
     * Method: isNotExists(String filePath)
     */
    @Test
    public void testNotExistsFilePath() throws Exception {
        System.out.println(FileUtil.isNotExists(filePath));
    }

    /**
     * Method: isDirectory(File file)
     */
    @Test
    public void testIsDirectoryFile() throws Exception {
        File file = new File(filePath);
        System.out.println(FileUtil.isDirectory(file));
    }

    /**
     * Method: isDirectory(String filePath)
     */
    @Test
    public void testIsDirectoryFilePath() throws Exception {
        System.out.println(FileUtil.isDirectory(filePath));
    }

    /**
     * Method: isNotDirectory(File file)
     */
    @Test
    public void testIsNotDirectoryFile() throws Exception {
        File file = new File(filePath);
        System.out.println(FileUtil.isNotDirectory(file));
    }

    /**
     * Method: isNotDirectory(String filePath)
     */
    @Test
    public void testIsNotDirectoryFilePath() throws Exception {
        System.out.println(FileUtil.isNotDirectory(filePath));
    }

    @Test
    public void getCanonicalPath() {
        File file = new File(filePath);
        System.out.println(FileUtil.getCanonicalPath(file));
    }

    @Test
    public void testGetCanonicalPath() {
        System.out.println(FileUtil.getCanonicalPath(filePath));
    }

    @Test
    public void getSuffix() {
        System.out.println(FileUtil.getSuffix(filePath));
    }

    /**
     * Method: getSuffix(File file)
     */
    @Test
    public void testGetSuffix() throws Exception {
        File file = new File(filePath);
        System.out.println(FileUtil.getSuffix(file));
    }

    /**
     * Method: mkDir(File file)
     */
    @Test
    public void testMkDirFile() throws Exception {
        FileUtil.mkDir(new File(basePath + "/a/a"));
    }

    /**
     * Method: mkDir(String path)
     */
    @Test
    public void testMkDirPath() throws Exception {
        FileUtil.mkDir(basePath + "/a/b");
    }

    /**
     * Method: deleteFile(File file)
     */
    @Test
    public void testDeleteFile() throws Exception {
        File file = new File(basePath + "/a/a");
        FileUtil.deleteFile(file);
    }

    /**
     * Method: deleteFile(String filePath)
     */
    @Test
    public void testDeleteFileFilePath() throws Exception {
        FileUtil.deleteFile(basePath + "/a/b");
    }

    /**
     * Method: rmFile(File file, boolean isRmChilds)
     */
    @Test
    public void testRmFileForFileIsRmChilds() throws Exception {
        File file = new File(basePath + "/a");
        FileUtil.rmFile(file, true);
    }

    /**
     * Method: rmFile(File file)
     */
    @Test
    public void testRmFileFile() throws Exception {
        try {
            File file = new File(basePath + "/a");
            FileUtil.rmFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method: rmFile(String filePath, boolean isRmChilds)
     */
    @Test
    public void testRmFileForFilePathIsRmChilds() throws Exception {
        FileUtil.rmFile(basePath + "/a", true);
    }

    /**
     * Method: rmFile(String filePath)
     */
    @Test
    public void testRmFileFilePath() throws Exception {
        try {
            FileUtil.rmFile(basePath + "/a");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method: clearDir(File file, boolean rmSelf)
     */
    @Test
    public void testClearDirForFileRmSelf() throws Exception {
        FileUtil.mkDir(basePath + "/a/b/c");
        File file = new File(basePath + "/a/b");
        FileUtil.clearDir(file, true);
    }

    /**
     * Method: clearDir(File file)
     */
    @Test
    public void testClearDirFile() throws Exception {
        FileUtil.mkDir(basePath + "/a/b/c");
        File file = new File(basePath + "/a/b");
        FileUtil.clearDir(file);
    }

    /**
     * Method: clearDir(String filePath, boolean rmSelf)
     */
    @Test
    public void testClearDirForFilePathRmSelf() throws Exception {
        FileUtil.mkDir(basePath + "/a/b/c");
        FileUtil.clearDir(basePath + "/a/b", true);
    }

    /**
     * Method: clearDir(String filePath)
     */
    @Test
    public void testClearDirFilePath() throws Exception {
        FileUtil.mkDir(basePath + "/a/b/c");
        FileUtil.clearDir(basePath + "/a/b");
    }

    /**
     * Method: createFile(File file)
     */
    @Test
    public void testCreateFileFile() throws Exception {
        FileUtil.createFile(new File(basePath + "/a/b/c.txt"));
    }

    /**
     * Method: createFile(String path)
     */
    @Test
    public void testCreateFilePath() throws Exception {
        FileUtil.createFile(basePath + "/a/b/c/d.txt");
    }

    /**
     * Method: copyTo(File file, File copyFile)
     */
    @Test
    public void testCopyToForFileCopyFile() throws Exception {
        FileUtil.copyTo(new File(filePath), new File(filePathCopy));
    }

    /**
     * Method: copyTo(String filePath, String copyFilePath)
     */
    @Test
    public void testCopyToForFilePathCopyFilePath() throws Exception {
        FileUtil.copyTo(filePath, filePathCopy);
    }

    /**
     * Method: copyTo(File file, String copyFilePath)
     */
    @Test
    public void testCopyToForFileCopyFilePath() throws Exception {
        FileUtil.copyTo(new File(filePath), filePathCopy);
    }

    /**
     * Method: copyTo(String filePath, File copyFile)
     */
    @Test
    public void testCopyToForFilePathCopyFile() throws Exception {
        FileUtil.copyTo(filePath, new File(filePathCopy));
    }

    /**
     * Method: copyToDir(File file, File targetDir, boolean containsRoot)
     */
    @Test
    public void testCopyToDirForFileTargetDirContainsRoot() throws Exception {
        //文件
        FileUtil.copyToDir(new File(filePath), new File(basePath + "/a/f"), true);

        //文件夹
        String fileDir = new File(filePath).getParentFile().getParent();
        FileUtil.copyToDir(new File(fileDir), new File(basePath + "/b"), true);//包含自身
        FileUtil.copyToDir(new File(fileDir), new File(basePath + "/c"), false);//不包含自身
    }

    /**
     * Method: copyToDir(String filePath, String targetDirPath, boolean containsRoot)
     */
    @Test
    public void testCopyToDirForFilePathTargetDirPathContainsRoot() throws Exception {
        //文件
        FileUtil.copyToDir(filePath, basePath + "/a/f", true);

        //文件夹
        String fileDir = new File(filePath).getParentFile().getParent();
        FileUtil.copyToDir(fileDir, basePath + "/b", true);//包含自身
        FileUtil.copyToDir(fileDir, basePath + "/c", false);//不包含自身
    }

    /**
     * Method: copyToDir(String filePath, String targetDirPath)
     */
    @Test
    public void testCopyToDirForFilePathTargetDirPath() throws Exception {
        //文件
        FileUtil.copyToDir(filePath, basePath + "/a/f");

        //文件夹
        String fileDir = new File(filePath).getParentFile().getParent();
        FileUtil.copyToDir(fileDir, basePath + "/b");
    }

    /**
     * Method: copyToDir(File file, String targetDirPath, boolean containsRoot)
     */
    @Test
    public void testCopyToDirForFileTargetDirPathContainsRoot() throws Exception {
        //文件
        FileUtil.copyToDir(new File(filePath), basePath + "/a/f", true);

        //文件夹
        String fileDir = new File(filePath).getParentFile().getParent();
        FileUtil.copyToDir(new File(fileDir), basePath + "/b", true);//包含自身
        FileUtil.copyToDir(new File(fileDir), basePath + "/c", false);//包含自身
    }

    /**
     * Method: copyToDir(File file, String targetDirPath)
     */
    @Test
    public void testCopyToDirForFileTargetDirPath() throws Exception {
        //文件
        FileUtil.copyToDir(new File(filePath), basePath + "/a/f");

        //文件夹
        String fileDir = new File(filePath).getParentFile().getParent();
        FileUtil.copyToDir(new File(fileDir), basePath + "/b");
    }

    /**
     * Method: copyToDir(String filePath, File targetDir, boolean containsRoot)
     */
    @Test
    public void testCopyToDirForFilePathTargetDirContainsRoot() throws Exception {
        //文件
        FileUtil.copyToDir(filePath, new File(basePath + "/a/f"), true);

        //文件夹
        String fileDir = new File(filePath).getParentFile().getParent();
        FileUtil.copyToDir(fileDir, new File(basePath + "/b"), true);//包含自身
        FileUtil.copyToDir(fileDir, new File(basePath + "/c"), false);//包含自身
    }

    /**
     * Method: copyToDir(String filePath, File targetDir)
     */
    @Test
    public void testCopyToDirForFilePathTargetDir() throws Exception {
        //文件
        FileUtil.copyToDir(filePath, new File(basePath + "/a/f"));

        //文件夹
        String fileDir = new File(filePath).getParentFile().getParent();
        FileUtil.copyToDir(fileDir, new File(basePath + "/b"));//包含自身
        FileUtil.copyToDir(fileDir, new File(basePath + "/c"));//包含自身
    }

    /**
     * Method: read(InputStream is)
     */
    @Test
    public void testReadIs() throws Exception {
        byte[] content = FileUtil.read(new FileInputStream(filePath));
        System.out.println(new String(content, FileUtil.ENCODING_UTF_8));
    }

    /**
     * Method: read(File file)
     */
    @Test
    public void testReadFile() throws Exception {
        byte[] content = FileUtil.read(new File(filePath));
        System.out.println(new String(content, FileUtil.ENCODING_UTF_8));
    }

    /**
     * Method: read(String filePath)
     */
    @Test
    public void testReadFilePath() throws Exception {
        byte[] content = FileUtil.read(filePath);
        System.out.println(new String(content, FileUtil.ENCODING_UTF_8));
    }

    /**
     * Method: readToString(InputStream is, String encoding)
     */
    @Test
    public void testReadToStringForIsEncoding() throws Exception {
        String content = FileUtil.readToString(new FileInputStream(filePath), FileUtil.ENCODING_UTF_8);
        System.out.println(content);
    }

    /**
     * Method: readToString(InputStream is)
     */
    @Test
    public void testReadToStringIs() throws Exception {
        String content = FileUtil.readToString(new FileInputStream(filePath));
        System.out.println(content);
    }

    /**
     * Method: readToString(File file, String encoding)
     */
    @Test
    public void testReadToStringForFileEncoding() throws Exception {
        String content = FileUtil.readToString(new File(filePath), FileUtil.ENCODING_UTF_8);
        System.out.println(content);
    }

    /**
     * Method: readToString(File file)
     */
    @Test
    public void testReadToStringFile() throws Exception {
        String content = FileUtil.readToString(new File(filePath));
        System.out.println(content);
    }

    /**
     * Method: readToString(String filePath, String encoding)
     */
    @Test
    public void testReadToStringForFilePathEncoding() throws Exception {
        String content = FileUtil.readToString(filePath, FileUtil.ENCODING_UTF_8);
        System.out.println(content);
    }

    /**
     * Method: readToString(String filePath)
     */
    @Test
    public void testReadToStringFilePath() throws Exception {
        String content = FileUtil.readToString(filePath);
        System.out.println(content);
    }

    /**
     * Method: write(InputStream is, OutputStream os)
     */
    @Test
    public void testWriteForIsOs() throws Exception {
        FileUtil.write(new FileInputStream(filePath), new FileOutputStream(filePathCopy));
    }

    /**
     * Method: write(OutputStream os, byte[] content)
     */
    @Test
    public void testWriteForOsContent() throws Exception {
        FileUtil.write(new FileOutputStream(filePathCopy), FileUtil.read(filePath));
    }

    /**
     * Method: write(OutputStream os, String content, String encoding)
     */
    @Test
    public void testWriteForOsContentEncoding() throws Exception {
        FileUtil.write(new FileOutputStream(filePathCopy), FileUtil.readToString(filePath), FileUtil.ENCODING_UTF_8);
    }

    /**
     * Method: write(File file, byte[] content)
     */
    @Test
    public void testWriteForFileContent() throws Exception {
        FileUtil.write(new File(filePathCopy), FileUtil.read(filePath));
    }

    /**
     * Method: write(File file, String content, String encoding)
     */
    @Test
    public void testWriteForFileContentEncoding() throws Exception {
        FileUtil.write(new File(filePathCopy), FileUtil.readToString(filePath), FileUtil.ENCODING_UTF_8);
    }

    /**
     * Method: write(String filePath, byte[] content)
     */
    @Test
    public void testWriteForFilePathContent() throws Exception {
        FileUtil.write(filePathCopy, FileUtil.read(filePath));
    }

    /**
     * Method: write(String filePath, String content, String encoding)
     */
    @Test
    public void testWriteForFilePathContentEncoding() throws Exception {
        FileUtil.write(filePathCopy, FileUtil.readToString(filePath), FileUtil.ENCODING_UTF_8);
    }
}
