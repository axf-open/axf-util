package com.axinfu.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;

/**
 * SerializableUtil Tester.
 */
@SuppressWarnings("all")
public class SerializableUtilTest implements Serializable {
    private static final long serialVersionUID = 1L;

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void obj2bytes() {
        Demo demo = new Demo("zhangjianing", 29);
        byte[] bytes = SerializableUtil.obj2bytes(demo);
        System.out.println(bytes.length);
    }

    @Test
    public void bytes2obj() {
        Demo demo = new Demo("zhangjianing", 29);
        byte[] bytes = SerializableUtil.obj2bytes(demo);
        System.out.println(bytes.length);
        Demo demo2 = SerializableUtil.bytes2obj(bytes);
        System.out.println(demo2);
    }

    static class Demo implements Serializable {
        private static final long serialVersionUID = 1L;

        String name;
        int age;

        Demo(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Demo{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
