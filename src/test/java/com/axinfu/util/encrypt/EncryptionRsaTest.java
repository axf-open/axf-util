package com.axinfu.util.encrypt;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * EncryptionRSA Tester.
 */
@SuppressWarnings("all")
public class EncryptionRsaTest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: generatorRsaKey(int keySize, byte[] seed)
     */
    @Test
    public void testGeneratorRSAKeyForKeySizeSeed() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, "mykey".getBytes());

        System.out.println(encryptionKeyRSA.getModulus().toString());
        System.out.println(encryptionKeyRSA.getModulusHex());
        System.out.println(encryptionKeyRSA.getPublicExponent().toString());
        System.out.println(encryptionKeyRSA.getPublicExponentHex());
        System.out.println(encryptionKeyRSA.getPrivateExponent().toString());
        System.out.println(encryptionKeyRSA.getPrivateExponentHex());
        System.out.println(encryptionKeyRSA.getPublicKeyHex());
        System.out.println(encryptionKeyRSA.getPublicKeyBase64());
        System.out.println(encryptionKeyRSA.getPrivateKeyHex());
        System.out.println(encryptionKeyRSA.getPrivateKeyBase64());
    }

    /**
     * Method: generatorRsaKey(int keySize)
     */
    @Test
    public void testGeneratorRSAKeyKeySize() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024);

        System.out.println(encryptionKeyRSA.getModulus().toString());
        System.out.println(encryptionKeyRSA.getModulusHex());
        System.out.println(encryptionKeyRSA.getPublicExponent().toString());
        System.out.println(encryptionKeyRSA.getPublicExponentHex());
        System.out.println(encryptionKeyRSA.getPrivateExponent().toString());
        System.out.println(encryptionKeyRSA.getPrivateExponentHex());
        System.out.println(encryptionKeyRSA.getPublicKeyHex());
        System.out.println(encryptionKeyRSA.getPublicKeyBase64());
        System.out.println(encryptionKeyRSA.getPrivateKeyHex());
        System.out.println(encryptionKeyRSA.getPrivateKeyBase64());
    }

    /**
     * Method: parseRsaPublicKey(BigInteger modulus, BigInteger exponent)
     */
    @Test
    public void testParseRSAPublicKey() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, "mykey".getBytes());
        RSAPublicKey publicKey = EncryptionRsa.parseRsaPublicKey(encryptionKeyRSA.getModulus(),
                encryptionKeyRSA.getPublicExponent());
        System.out.println(publicKey.toString());
    }

    /**
     * Method: parseRsaPublicKeyFromHex(String publicKeyHex)
     */
    @Test
    public void testParseRSAPublicKeyFromHex() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, "mykey".getBytes());
        RSAPublicKey publicKey = EncryptionRsa.parseRsaPublicKeyFromHex(encryptionKeyRSA.getPublicKeyHex());
        System.out.println(publicKey.toString());
    }

    /**
     * Method: parseRsaPublicKeyFromBase64(String publicKeyBase64)
     */
    @Test
    public void testParseRSAPublicKeyFromBase64() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, "mykey".getBytes());
        RSAPublicKey publicKey = EncryptionRsa.parseRsaPublicKeyFromBase64(encryptionKeyRSA.getPublicKeyBase64());
        System.out.println(publicKey.toString());
    }

    /**
     * Method: parseRsaPrivateKey(BigInteger modulus, BigInteger exponent)
     */
    @Test
    public void testParseRSAPrivateKey() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, "mykey".getBytes());
        RSAPrivateKey privateKey = EncryptionRsa.parseRsaPrivateKey(encryptionKeyRSA.getModulus(),
                encryptionKeyRSA.getPrivateExponent());
        System.out.println(privateKey.toString());
    }

    /**
     * Method: parseRsaPrivateKeyFromHex(String primaryKeyHex)
     */
    @Test
    public void testParseRSAPrivateKeyFromHex() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, "mykey".getBytes());
        RSAPrivateKey privateKey = EncryptionRsa.parseRsaPrivateKeyFromHex(encryptionKeyRSA.getPrivateKeyHex());
        System.out.println(privateKey.toString());
    }

    /**
     * Method: parseRsaPrivateKeyFromBase64(String primaryKeyHex)
     */
    @Test
    public void testParseRSAPrivateKeyFromBase64() throws Exception {
        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, "mykey".getBytes());
        RSAPrivateKey privateKey = EncryptionRsa.parseRsaPrivateKeyFromBase64(encryptionKeyRSA.getPrivateKeyBase64());
        System.out.println(privateKey.toString());
    }

    /**
     * Method: encryptRsa(Key key, byte[] data)
     */
    @Test
    public void testEncryptRSA() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptRsaToHex(Key key, byte[] data)
     */
    @Test
    public void testEncryptRSAToHexForKeyData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptRsaToHex(Key key, String data, String encoding)
     */
    @Test
    public void testEncryptRSAToHexForKeyDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptRsaToBase64(Key key, byte[] data)
     */
    @Test
    public void testEncryptRSAToBase64ForKeyData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptRsaToBase64(Key key, String data, String encoding)
     */
    @Test
    public void testEncryptRSAToBase64ForKeyDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptRsa(Key key, byte[] data)
     */
    @Test
    public void testDecryptRSA() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptRsaFromHex(Key key, String data)
     */
    @Test
    public void testDecryptRSAFromHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptRsaFromHexToString(Key key, String data, String encoding)
     */
    @Test
    public void testDecryptRSAFromHexToStringForKeyDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptRsaFromHexToString(Key key, String data)
     */
    @Test
    public void testDecryptRSAFromHexToStringForKeyData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptRsaFromBase64(Key key, String data)
     */
    @Test
    public void testDecryptRSAFromBase64() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptRsaFromBase64ToString(Key key, String data, String encoding)
     */
    @Test
    public void testDecryptRSAFromBase64ToStringForKeyDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptRsaFromBase64ToString(Key key, String data)
     */
    @Test
    public void testDecryptRSAFromBase64ToStringForKeyData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: signRsa(String signAlgorithms, PrivateKey privateKey, byte[] data)
     */
    @Test
    public void testSignRSA() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: signRsaToHex(String signAlgorithms, PrivateKey privateKey, byte[] data)
     */
    @Test
    public void testSignRSAToHexForSignAlgorithmsPrivateKeyData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: signRsaToHex(String signAlgorithms, PrivateKey privateKey, String data, String encoding)
     */
    @Test
    public void testSignRSAToHexForSignAlgorithmsPrivateKeyDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: signRsaToBase64(String signAlgorithms, PrivateKey privateKey, byte[] data)
     */
    @Test
    public void testSignRSAToBase64ForSignAlgorithmsPrivateKeyData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: signRsaToBase64(String signAlgorithms, PrivateKey privateKey, String data, String encoding)
     */
    @Test
    public void testSignRSAToBase64ForSignAlgorithmsPrivateKeyDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: verifyRsa(String signAlgorithms, PublicKey publicKey, byte[] data, byte[] sign)
     */
    @Test
    public void testVerifyRSA() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: verifyRsaFromHex(String signAlgorithms, PublicKey publicKey, byte[] data, String signHex)
     */
    @Test
    public void testVerifyRSAFromHexForSignAlgorithmsPublicKeyDataSignHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: verifyRsaFromHex(String signAlgorithms, PublicKey publicKey, String data, String encoding, String
     * signHex)
     */
    @Test
    public void testVerifyRSAFromHexForSignAlgorithmsPublicKeyDataEncodingSignHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: verifyRsaFromBase64(String signAlgorithms, PublicKey publicKey, byte[] data, String signBase64)
     */
    @Test
    public void testVerifyRSAFromBase64ForSignAlgorithmsPublicKeyDataSignBase64() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: verifyRsaFromBase64(String signAlgorithms, PublicKey publicKey, String data, String encoding, String
     * signBase64)
     */
    @Test
    public void testVerifyRSAFromBase64ForSignAlgorithmsPublicKeyDataEncodingSignBase64() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getModulus()
     */
    @Test
    public void testGetModulus() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getModulusHex()
     */
    @Test
    public void testGetModulusHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPublicExponent()
     */
    @Test
    public void testGetPublicExponent() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPublicExponentHex()
     */
    @Test
    public void testGetPublicExponentHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPrivateExponent()
     */
    @Test
    public void testGetPrivateExponent() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPrivateExponentHex()
     */
    @Test
    public void testGetPrivateExponentHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPublicKey()
     */
    @Test
    public void testGetPublicKey() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: setPublicKey(byte[] publicKey)
     */
    @Test
    public void testSetPublicKey() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPublicKeyHex()
     */
    @Test
    public void testGetPublicKeyHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPublicKeyBase64()
     */
    @Test
    public void testGetPublicKeyBase64() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPrivateKey()
     */
    @Test
    public void testGetPrivateKey() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: setPrivateKey(byte[] privateKey)
     */
    @Test
    public void testSetPrivateKey() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPrivateKeyHex()
     */
    @Test
    public void testGetPrivateKeyHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getPrivateKeyBase64()
     */
    @Test
    public void testGetPrivateKeyBase64() throws Exception {
        //TODO: Test goes here...
    }

    @Test
    public void test() throws Exception {
        String sourceStr = "hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello " +
                "世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello " +
                "世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!";
        byte[] sourceBytes = sourceStr.getBytes(Encryption.ENCODING_UTF8);
        String mykeyStr = "mykey";
        byte[] mykeyBytes = mykeyStr.getBytes(Encryption.ENCODING_UTF8);
        byte[] encodeBytes;
        String encodeStr;
        byte[] decodeBytes;
        String decodeStr;
        byte[] signBytes;
        String signHex;
        String signBase64;
        boolean verify;

        EncryptionRsa.EncryptionKeyRsa encryptionKeyRSA = EncryptionRsa.generatorRsaKey(1024, mykeyBytes);
        System.out.println("RSA-base64公钥：\t" + encryptionKeyRSA.getPublicKeyBase64());
        System.out.println("RSA-base64私钥：\t" + encryptionKeyRSA.getPrivateKeyBase64());

        RSAPublicKey publicKey = EncryptionRsa.parseRsaPublicKey(encryptionKeyRSA.getModulus(),
                encryptionKeyRSA.getPublicExponent());
        RSAPrivateKey privateKey = EncryptionRsa.parseRsaPrivateKey(encryptionKeyRSA.getModulus(),
                encryptionKeyRSA.getPrivateExponent());

        /*
            hex
         */
        //公钥加密-hex
        encodeBytes = EncryptionRsa.encryptRsa(publicKey, sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("RSA-hex公钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToHex(publicKey, sourceBytes);
        System.out.println("RSA-hex公钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToHex(publicKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex公钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToHex(publicKey, sourceStr);
        System.out.println("RSA-hex公钥加密后4：\t" + encodeStr);

        //私钥解密
        decodeBytes = EncryptionRsa.decryptRsa(privateKey, encodeBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA私钥解密后：\t" + decodeStr);

        //私钥解密-hex
        decodeBytes = EncryptionRsa.decryptRsaFromHex(privateKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex私钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromHexToString(privateKey, encodeStr);
        System.out.println("RSA-hex私钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromHexToString(privateKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex私钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromHexToString(privateKey, encodeStr);
        System.out.println("RSA-hex私钥解密后4：\t" + decodeStr);

        //私钥加密-hex
        encodeBytes = EncryptionRsa.encryptRsa(privateKey, sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("RSA-hex私钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToHex(privateKey, sourceBytes);
        System.out.println("RSA-hex私钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToHex(privateKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex私钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToHex(privateKey, sourceStr);
        System.out.println("RSA-hex私钥加密后4：\t" + encodeStr);

        //公钥解密-hex
        decodeBytes = EncryptionRsa.decryptRsaFromHex(publicKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex公钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromHexToString(publicKey, encodeStr);
        System.out.println("RSA-hex公钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromHexToString(publicKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex公钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromHexToString(publicKey, encodeStr);
        System.out.println("RSA-hex公钥解密后4：\t" + decodeStr);

        /*
            base64
         */
        //公钥加密-base64
        encodeBytes = EncryptionRsa.encryptRsa(publicKey, sourceBytes);
        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("RSA-base64公钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToBase64(publicKey, sourceBytes);
        System.out.println("RSA-base64公钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToBase64(publicKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64公钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToBase64(publicKey, sourceStr);
        System.out.println("RSA-base64公钥加密后4：\t" + encodeStr);

        //私钥解密
        decodeBytes = EncryptionRsa.decryptRsa(privateKey, encodeBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA私钥解密后：\t" + decodeStr);

        //私钥解密-base64
        decodeBytes = EncryptionRsa.decryptRsaFromBase64(privateKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64私钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromBase64ToString(privateKey, encodeStr);
        System.out.println("RSA-base64私钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromBase64ToString(privateKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64私钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromBase64ToString(privateKey, encodeStr);
        System.out.println("RSA-base64私钥解密后4：\t" + decodeStr);

        //私钥加密-base64
        encodeBytes = EncryptionRsa.encryptRsa(privateKey, sourceBytes);
        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("RSA-base64私钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToBase64(privateKey, sourceBytes);
        System.out.println("RSA-base64私钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToBase64(privateKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64私钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRsa.encryptRsaToBase64(privateKey, sourceStr);
        System.out.println("RSA-base64私钥加密后4：\t" + encodeStr);

        //公钥解密-base64
        decodeBytes = EncryptionRsa.decryptRsaFromBase64(publicKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64公钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromBase64ToString(publicKey, encodeStr);
        System.out.println("RSA-base64公钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromBase64ToString(publicKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64公钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRsa.decryptRsaFromBase64ToString(publicKey, encodeStr);
        System.out.println("RSA-base64公钥解密后4：\t" + decodeStr);

        /*
            签名、签名验证
         */
        signBytes = EncryptionRsa.signRsa(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-hex签名1：\t" + Encryption.bytesToHex(signBytes));
        signHex = EncryptionRsa.signRsaToHex(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-hex签名2：\t" + signHex);
        signHex = EncryptionRsa.signRsaToHex(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceStr,
                Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex签名3：\t" + signHex);
        signHex = EncryptionRsa.signRsaToHex(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceStr);
        System.out.println("RSA-hex签名4：\t" + signHex);

        signBytes = EncryptionRsa.signRsa(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-base64签名1：\t" + Encryption.encryptBase64(signBytes));
        signBase64 = EncryptionRsa.signRsaToBase64(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-base64签名2：\t" + signBase64);
        signBase64 = EncryptionRsa.signRsaToBase64(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceStr,
                Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64签名3：\t" + signBase64);
        signBase64 = EncryptionRsa.signRsaToBase64(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceStr);
        System.out.println("RSA-base64签名4：\t" + signBase64);

        verify = EncryptionRsa.verifyRsa(EncryptionRsa.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes, signBytes);
        System.out.println("RSA验签：\t" + verify);
        verify = EncryptionRsa.verifyRsaFromHex(EncryptionRsa.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes, signHex);
        System.out.println("RSA验签-hex：\t" + verify);
        verify = EncryptionRsa.verifyRsaFromBase64(EncryptionRsa.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes,
                signBase64);
        System.out.println("RSA验签-base64：\t" + verify);
    }

    @Test
    public void test2() throws Exception {
        String sourceStr = "data";
        byte[] sourceBytes = sourceStr.getBytes(Encryption.ENCODING_UTF8);
        byte[] signBytes;
        boolean verify;
        PublicKey publicKey = EncryptionRsa.parseRsaPublicKeyFromBase64(
                "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC6JvQceir16JeV1gDoDKEsAUNJKWp9HkxImEdpRYaFTrYiHfG01LNpt5nPUjH4uiabGul5Nw3OmxsQm3rpIU8nEevN3rtHFtOSPtv0n/hLtt8WQZf3DVpnU0TxbErIYEq3UYRXxxfreLqspLPv7Dobu6NAI5xnuSAHbptuC2UKEQIDAQAB");
        PrivateKey privateKey = EncryptionRsa.parseRsaPrivateKeyFromBase64(
                "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALom9Bx6KvXol5XWAOgMoSwBQ0kpan0eTEiYR2lFhoVOtiId8bTUs2m3mc9SMfi6Jpsa6Xk3Dc6bGxCbeukhTycR683eu0cW05I+2/Sf+Eu23xZBl/cNWmdTRPFsSshgSrdRhFfHF+t4uqyks+/sOhu7o0AjnGe5IAdum24LZQoRAgMBAAECgYBNCmAPBRSQj0FlFptrbgaqAp/JQKW8wHRLuxIa5FZHB4tjVUc6UqkqAH1ciyT5Tnk5ygqESx+guRqbZe2ZXFq4GM7rjHkpYl8UnbBaDudp/499vLB2VCFxkk16+6MpHKnSOxNRP+j53satybAAIclyvLlMExN7QRLzxodZKAF3XQJBAOU70RaeiTLuRlR7zXultRfZhQ4u3TYxlMJ8PfgPz6aC3d42Oh/5CV/4wXgn5HFr/zGOwZM9uhhZDIhSIsrsf9MCQQDP41uT/uYLwmvlMbVgnuFr4SG/30ev4O6x09+bLBR8Zx/09Nf095Vaz/BcmbHQ7QIU6Q91xFZHcKg5LITcasQLAkA3xaLsduv4iUJxQaHP6JQz1kdqGPrXOZ7w5puJJAeogoSKkPT5XHTsdbBUlJgfBGCVZR8xvL3vOJM1A47Vgk7jAkEAlrLl6/7XnKavuFG0ffouxxlIceLWALU500cXzVDC+Pt4uwXSlw3zAwXB5B62PBHTdH0Oa/yL3vXXvLp9BZuPyQJBAOILAApgyN7Q599rjhBKvdBpDm9hUEWNnN+Hs/ZpKRLfAQm4EPV+uiClS5NW8U+c35+8yzWO/u/zuu23Y7xWd1M=");
        signBytes = EncryptionRsa.signRsa(EncryptionRsa.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        verify = EncryptionRsa.verifyRsa(EncryptionRsa.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes, signBytes);
        System.out.println(String.valueOf(verify));
    }
}
