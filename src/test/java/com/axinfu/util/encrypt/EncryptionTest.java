package com.axinfu.util.encrypt;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Encryption Tester.
 */
@SuppressWarnings("all")
public class EncryptionTest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: encryptBase64(byte[] data)
     */
    @Test
    public void testEncryptBASE64Data() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptBase64(String data, String encoding)
     */
    @Test
    public void testEncryptBASE64ForDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptBase64ToString(byte[] data)
     */
    @Test
    public void testEncryptBASE64ToStringData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptBase64ToString(String data, String encoding)
     */
    @Test
    public void testEncryptBASE64ToStringForDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptBase64(byte[] data)
     */
    @Test
    public void testDecryptBASE64Data() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptBase64(String data, String encoding)
     */
    @Test
    public void testDecryptBASE64ForDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptBase64ToString(String data, String encoding, String stringEncoding)
     */
    @Test
    public void testDecryptBASE64ToStringForDataEncodingStringEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptBase64ToString(String data, String stringEncoding)
     */
    @Test
    public void testDecryptBASE64ToStringForDataStringEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptBase64ToString(String data)
     */
    @Test
    public void testDecryptBASE64ToStringData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptMd5(byte[] data)
     */
    @Test
    public void testEncryptMD5() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptMd5ToHex(byte[] data)
     */
    @Test
    public void testEncryptMD5ToHexData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptMd5ToHex(String data, String encoding)
     */
    @Test
    public void testEncryptMD5ToHexForDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptMd5ToBase64(byte[] data)
     */
    @Test
    public void testEncryptMD5ToBase64Data() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptMd5ToBase64(String data, String encoding)
     */
    @Test
    public void testEncryptMD5ToBase64ForDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptSha(String bit, byte[] data)
     */
    @Test
    public void testEncryptSHA() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptShaToHex(String bit, byte[] data)
     */
    @Test
    public void testEncryptSHAToHexForBitData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptShaToHex(String bit, String data, String encoding)
     */
    @Test
    public void testEncryptSHAToHexForBitDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptShaToBase64(String bit, byte[] data)
     */
    @Test
    public void testEncryptSHAToBase64ForBitData() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptShaToBase64(String bit, String data, String encoding)
     */
    @Test
    public void testEncryptSHAToBase64ForBitDataEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorHmacKey(int keySize, String hashName, byte[] seed)
     */
    @Test
    public void testGeneratorHMACKeyForKeySizeHashNameSeed() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorHmacKey(int keySize, String hashName)
     */
    @Test
    public void testGeneratorHMACKeyForKeySizeHashName() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptHmac(String hashName, byte[] data, byte[] key)
     */
    @Test
    public void testEncryptHMAC() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptHmacToHex(String hashName, byte[] data, byte[] key)
     */
    @Test
    public void testEncryptHMACToHexForHashNameDataKey() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptHmacToHex(String hashName, String data, byte[] key, String encoding)
     */
    @Test
    public void testEncryptHMACToHexForHashNameDataKeyEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptHmacToBase64(String hashName, byte[] data, byte[] key)
     */
    @Test
    public void testEncryptHMACToBase64ForHashNameDataKey() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptHmacToBase64(String hashName, String data, byte[] key, String encoding)
     */
    @Test
    public void testEncryptHMACToBase64ForHashNameDataKeyEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorDesKey(int keySize, byte[] seed)
     */
    @Test
    public void testGeneratorDESKeyForKeySizeSeed() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorDesKey(int keySize)
     */
    @Test
    public void testGeneratorDESKeyKeySize() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDes(String mode, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptDES() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesToHex(String mode, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptDESToHexForModePaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesToHex(String mode, String padding, String data, byte[] key, byte[] iv, String encoding)
     */
    @Test
    public void testEncryptDESToHexForModePaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesToBase64(String mode, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptDESToBase64ForModePaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesToBase64(String mode, String padding, String data, byte[] key, byte[] iv, String encoding)
     */
    @Test
    public void testEncryptDESToBase64ForModePaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDes(String mode, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDES() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesFromHex(String mode, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESFromHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesFromHexToString(String mode, String padding, String data, byte[] key, byte[] iv, String
     * encoding)
     */
    @Test
    public void testDecryptDESFromHexToStringForModePaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesFromHexToString(String mode, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESFromHexToStringForModePaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesFromBase64(String mode, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESFromBase64() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesFromBase64ToString(String mode, String padding, String data, byte[] key, byte[] iv, String
     * encoding)
     */
    @Test
    public void testDecryptDESFromBase64ToStringForModePaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesFromBase64ToString(String mode, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESFromBase64ToStringForModePaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorDesedeKey(int keySize, byte[] seed)
     */
    @Test
    public void testGeneratorDESedeKeyForKeySizeSeed() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorDesedeKey(int keySize)
     */
    @Test
    public void testGeneratorDESedeKeyKeySize() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesede(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptDESede() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesedeToHex(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptDESedeToHexForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesedeToHex(String algorithm, String padding, String data, byte[] key, byte[] iv, String encoding)
     */
    @Test
    public void testEncryptDESedeToHexForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesedeToBase64(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptDESedeToBase64ForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptDesedeToBase64(String algorithm, String padding, String data, byte[] key, byte[] iv, String
     * encoding)
     */
    @Test
    public void testEncryptDESedeToBase64ForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesede(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESede() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesedeFromHex(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESedeFromHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesedeFromHexToString(String algorithm, String padding, String data, byte[] key, byte[] iv,
     * String encoding)
     */
    @Test
    public void testDecryptDESedeFromHexToStringForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesedeFromHexToString(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESedeFromHexToStringForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesedeFromBase64(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESedeFromBase64() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesedeFromBase64ToString(String algorithm, String padding, String data, byte[] key, byte[] iv,
     * String encoding)
     */
    @Test
    public void testDecryptDESedeFromBase64ToStringForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptDesedeFromBase64ToString(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptDESedeFromBase64ToStringForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorAesKey(int keySize, byte[] seed)
     */
    @Test
    public void testGeneratorAESKeyForKeySizeSeed() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: generatorAesKey(int keySize)
     */
    @Test
    public void testGeneratorAESKeyKeySize() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptAes(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptAES() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptAesToHex(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptAESToHexForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptAesToHex(String algorithm, String padding, String data, byte[] key, byte[] iv, String encoding)
     */
    @Test
    public void testEncryptAESToHexForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptAesToBase64(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testEncryptAESToBase64ForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: encryptAesToBase64(String algorithm, String padding, String data, byte[] key, byte[] iv, String encoding)
     */
    @Test
    public void testEncryptAESToBase64ForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptAes(String algorithm, String padding, byte[] data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptAES() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptAesFromHex(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptAESFromHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptAesFromHexToString(String algorithm, String padding, String data, byte[] key, byte[] iv, String
     * encoding)
     */
    @Test
    public void testDecryptAESFromHexToStringForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptAesFromHexToString(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptAESFromHexToStringForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptAesFromBase64(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptAESFromBase64() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptAesFromBase64ToString(String algorithm, String padding, String data, byte[] key, byte[] iv,
     * String encoding)
     */
    @Test
    public void testDecryptAESFromBase64ToStringForAlgorithmPaddingDataKeyIvEncoding() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: decryptAesFromBase64ToString(String algorithm, String padding, String data, byte[] key, byte[] iv)
     */
    @Test
    public void testDecryptAESFromBase64ToStringForAlgorithmPaddingDataKeyIv() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: bytesToHex(byte[] bytes)
     */
    @Test
    public void testBytesToHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: hexToBytes(String hexstr)
     */
    @Test
    public void testHexToBytes() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getKey()
     */
    @Test
    public void testGetKey() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getKeyHex()
     */
    @Test
    public void testGetKeyHex() throws Exception {
        //TODO: Test goes here...
    }

    /**
     * Method: getKeyBase64()
     */
    @Test
    public void testGetKeyBase64() throws Exception {
        //TODO: Test goes here...
    }

    @Test
    public void test() throws Exception {
        String sourceStr = "hello 世界!";
        byte[] sourceBytes = sourceStr.getBytes(Encryption.ENCODING_UTF8);
        String mykeyStr = "mykey";
        byte[] mykeyBytes = mykeyStr.getBytes(Encryption.ENCODING_UTF8);
        String keyStr;
        byte[] keyBytes;
        String ivStr = "12345678";
        byte[] ivBytes = ivStr.getBytes(Encryption.ENCODING_UTF8);
        String ivStrL = "1234567812345678";
        byte[] ivBytesL = ivStrL.getBytes(Encryption.ENCODING_UTF8);
        byte[] encodeBytes;
        String encodeStr;
        byte[] decodeBytes;
        String decodeStr;

        //原始数据
        System.out.println("原始数据：\t" + sourceStr);
        System.out.println("----------------------------------------");

        /*
            BASE64加密解密
        */
        //BASE64加密
        encodeStr = Encryption.encryptBase64ToString(sourceBytes);
        System.out.println("BASE64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptBase64ToString(sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("BASE64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptBase64ToString(sourceStr);
        System.out.println("BASE64加密后3：\t" + encodeStr);

        //BASE64解密
        decodeBytes = Encryption.decryptBase64(encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("BASE64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptBase64ToString(encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("BASE64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptBase64ToString(encodeStr);
        System.out.println("BASE64解密后3：\t" + decodeStr);

        System.out.println("----------------------------------------");

        /*
            MD5加密
        */
        encodeBytes = Encryption.encryptMd5(sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("MD5-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptMd5ToHex(sourceBytes);
        System.out.println("MD5-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptMd5ToHex(sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("MD5-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptMd5ToHex(sourceStr);
        System.out.println("MD5-hex加密后4：\t" + encodeStr);

        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("MD5-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptMd5ToBase64(sourceBytes);
        System.out.println("MD5-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptMd5ToBase64(sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("MD5-base64加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptMd5ToBase64(sourceStr);
        System.out.println("MD5-base64加密后4：\t" + encodeStr);

        System.out.println("----------------------------------------");

        /*
            SHA加密
        */
        encodeBytes = Encryption.encryptSha(Encryption.ALGORITHM_SHA_BIT_1, sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("SHA-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptShaToHex(Encryption.ALGORITHM_SHA_BIT_1, sourceBytes);
        System.out.println("SHA-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptShaToHex(Encryption.ALGORITHM_SHA_BIT_1, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("SHA-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptShaToHex(Encryption.ALGORITHM_SHA_BIT_1, sourceStr);
        System.out.println("SHA-hex加密后4：\t" + encodeStr);

        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("SHA-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptShaToBase64(Encryption.ALGORITHM_SHA_BIT_1, sourceBytes);
        System.out.println("SHA-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptShaToBase64(Encryption.ALGORITHM_SHA_BIT_1, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("SHA-base64加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptShaToBase64(Encryption.ALGORITHM_SHA_BIT_1, sourceStr);
        System.out.println("SHA-base64加密后4：\t" + encodeStr);

        System.out.println("----------------------------------------");

        /*
            HMAC带秘钥加密
        */
        Encryption.EncryptionKey hmacKey = Encryption.generatorHmacKey(1, Encryption.HAMC_ALGORITHM_MD5, mykeyBytes);
        keyBytes = hmacKey.getKey();
        System.out.println("HMAC-hex秘钥：\t" + hmacKey.getKeyHex());
        System.out.println("HMAC-base64秘钥：\t" + hmacKey.getKeyBase64());

        encodeBytes = Encryption.encryptHmac(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("HMAC-hex后1：\t" + encodeStr);

        encodeStr = Encryption.encryptHmacToHex(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        System.out.println("HMAC-hex后2：\t" + encodeStr);

        encodeStr = Encryption.encryptHmacToHex(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes,
                Encryption.ENCODING_UTF8);
        System.out.println("HMAC-hex后3：\t" + encodeStr);

        encodeStr = Encryption.encryptHmacToHex(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes);
        System.out.println("HMAC-hex后4：\t" + encodeStr);

        encodeBytes = Encryption.encryptHmac(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("HMAC-base64后1：\t" + encodeStr);

        encodeStr = Encryption.encryptHmacToBase64(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        System.out.println("HMAC-base64后2：\t" + encodeStr);

        encodeStr = Encryption.encryptHmacToBase64(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes,
                Encryption.ENCODING_UTF8);
        System.out.println("HMAC-base64后3：\t" + encodeStr);

        encodeStr = Encryption.encryptHmacToBase64(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes);
        System.out.println("HMAC-base64后4：\t" + encodeStr);

        System.out.println("----------------------------------------");

        /*
          DES带秘钥加密解密
        */
        Encryption.EncryptionKey desKey = Encryption.generatorDesKey(56, mykeyBytes);
        keyBytes = desKey.getKey();
        System.out.println("DES-hex秘钥：\t" + desKey.getKeyHex());
        System.out.println("DES-base64秘钥：\t" + desKey.getKeyBase64());
        System.out.println("DES-iv：\t" + ivStr);

        //加密-hex
        encodeBytes = Encryption.encryptDes(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("DES-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDesToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytes);
        System.out.println("DES-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDesToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptDesToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceStr, keyBytes, ivBytes);
        System.out.println("DES-hex加密后4：\t" + encodeStr);

        //解密
        decodeBytes = Encryption.decryptDes(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                encodeBytes, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES解密后1：\t" + decodeStr);

        //解密-hex
        decodeBytes = Encryption.decryptDesFromHex(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-hex解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDesFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-hex解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDesFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-hex解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDesFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-hex解密后4：\t" + decodeStr);

        //加密-base64
        encodeBytes = Encryption.encryptDes(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("DES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDesToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        System.out.println("DES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDesToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDesToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes);
        System.out.println("DES-base64加密后3：\t" + encodeStr);

        //解密-base64
        decodeBytes = Encryption.decryptDesFromBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-base64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDesFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-base64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDesFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-base64解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDesFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-base64解密后4：\t" + decodeStr);

        System.out.println("----------------------------------------");

        /*
          DESede带秘钥加密解密
        */
        Encryption.EncryptionKey deSedeKey = Encryption.generatorDesedeKey(112, mykeyBytes);
        keyBytes = deSedeKey.getKey();
        System.out.println("DESede-hex秘钥：\t" + deSedeKey.getKeyHex());
        System.out.println("DESede-base64秘钥：\t" + deSedeKey.getKeyBase64());
        System.out.println("DESede-iv：\t" + ivStr);

        //加密-hex
        encodeBytes = Encryption.encryptDesede(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("DESede-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDesedeToHex(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        System.out.println("DESede-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDesedeToHex(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptDesedeToHex(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes);
        System.out.println("DESede-hex加密后4：\t" + encodeStr);

        //解密
        decodeBytes = Encryption.decryptDesede(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                encodeBytes, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede解密后1：\t" + decodeStr);

        //解密-hex
        decodeBytes = Encryption.decryptDesedeFromHex(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-hex解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDesedeFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-hex解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDesedeFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-hex解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDesedeFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-hex解密后4：\t" + decodeStr);

        //加密-base64
        encodeBytes = Encryption.encryptDesede(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("DESede-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDesedeToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        System.out.println("DESede-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDesedeToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDesedeToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes);
        System.out.println("DESede-base64加密后3：\t" + encodeStr);

        //解密-base64
        decodeBytes = Encryption.decryptDesedeFromBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-base64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDesedeFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-base64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDesedeFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-base64解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDesedeFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-base64解密后4：\t" + decodeStr);

        System.out.println("----------------------------------------");

        /*
          AES带秘钥加密解密
        */
        Encryption.EncryptionKey aesKey = Encryption.generatorAesKey(128);
        keyBytes = aesKey.getKey();
        System.out.println("AES-hex秘钥：\t" + aesKey.getKeyHex());
        System.out.println("AES-base64秘钥：\t" + aesKey.getKeyBase64());
        System.out.println("AES-iv：\t" + ivStrL);

        //加密-hex
        encodeBytes = Encryption.encryptAes(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytesL);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("AES-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptAesToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytesL);
        System.out.println("AES-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptAesToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptAesToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceStr, keyBytes, ivBytesL);
        System.out.println("AES-hex加密后4：\t" + encodeStr);

        //解密
        decodeBytes = Encryption.decryptAes(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                encodeBytes, keyBytes, ivBytesL);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("AES解密后1：\t" + decodeStr);

        //解密-hex
        decodeBytes = Encryption.decryptAesFromHex(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("AES-hex解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptAesFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-hex解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptAesFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-hex解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptAesFromHexToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-hex解密后4：\t" + decodeStr);

        //加密-base64
        encodeBytes = Encryption.encryptAes(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING,
                sourceBytes, keyBytes, ivBytesL);
        encodeStr = Encryption.encryptBase64ToString(encodeBytes);
        System.out.println("AES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptAesToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytesL);
        System.out.println("AES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptAesToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptAesToBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytesL);
        System.out.println("AES-base64加密后3：\t" + encodeStr);

        //解密-base64
        decodeBytes = Encryption.decryptAesFromBase64(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("AES-base64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptAesFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-base64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptAesFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-base64解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptAesFromBase64ToString(Encryption.ENCRYPT_MODE_CBC,
                Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-base64解密后4：\t" + decodeStr);
    }
}
