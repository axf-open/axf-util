package com.axinfu.util.validq;

import com.axinfu.util.validq.validator.IsEmptyValidator;
import com.axinfu.util.validq.validator.IsNotEmptyValidator;
import com.axinfu.util.validq.validator.IsNotNullValidator;
import com.axinfu.util.validq.validator.IsNullValidator;
import com.axinfu.util.validq.validator.StringLengthValidator;
import com.axinfu.util.validq.validator.StringRegexValidator;
import org.junit.Test;

/**
 * ValidQ Tester.
 */
@SuppressWarnings("all")
public class ValidQTest {

    /**
     * Method: valid()
     */
    @Test
    public void testValid() throws Exception {
        ValidationResult result = ValidQ.create().failContinue()

                .on(null, new IsNotNullValidator().setField("field").setFieldName("fieldName")
                        .setShortField("shortField").setShortFieldName("shortFieldName"))
                .on(null, new IsNotNullValidator().setField("field").setShortField("shortField")
                        .setShortFieldName("shortFieldName"))
                .on(null, new IsNotNullValidator().setField("field").setShortField("shortField"))
                .on(null, new IsNotNullValidator().setShortField("shortField"))

                .on("asdf", new IsNullValidator().setField("a"))
                .on(null, new IsNullValidator().setField("a"))

                .on("asdf", new IsNotNullValidator().setField("b"))
                .on(null, new IsNotNullValidator().setField("b"))

                .on("asdf", new IsEmptyValidator().setField("c"))
                .on(null, new IsEmptyValidator().setField("c"))
                .on("  ", new IsEmptyValidator().setField("c"))

                .on("asdf", new IsNotEmptyValidator().setField("d"))
                .on(null, new IsNotEmptyValidator().setField("d"))
                .on("  ", new IsNotEmptyValidator().setField("d"))

                .on("1", new StringLengthValidator().setMin(2).setMax(3).setField("e"))
                .on("22", new StringLengthValidator().setMin(2).setMax(3).setField("e"))
                .on("333", new StringLengthValidator().setMin(2).setMax(3).setField("e"))
                .on("4444", new StringLengthValidator().setMin(2).setMax(3).setField("e"))

                .on("无效的验证", new StringLengthValidator().setMin(2).setMax(3).setField("f"), 1 > 2)
                .on("有效的验证(指定优先级)", new StringLengthValidator().setMin(2).setMax(3).setField("f"), 1, 1 < 2)

                .on("asdf(指定验证优先级)", new StringRegexValidator()
                        .setRegex("^[a-zA-Z0-9][a-zA-Z0-9_\\-\\.]{0,19}@(?:[a-zA-Z0-9\\-]+\\.)+[a-zA-Z]+$")
                        .setField("g"), 1)
                .on("jounzhang@126.com", new StringRegexValidator()
                        .setRegex("^[a-zA-Z0-9][a-zA-Z0-9_\\-\\.]{0,19}@(?:[a-zA-Z0-9\\-]+\\.)+[a-zA-Z]+$")
                        .setField("g"))

                .on("验证level:22,1", new StringLengthValidator()
                        .setMin(2).setMax(3).setField("h"), 22, 1, true)
                .on("验证level:333,1", new StringLengthValidator()
                        .setMin(2).setMax(3).setField("h"), 333, 1, true)
                .on("验证level:333,2", new StringLengthValidator()
                        .setMin(2).setMax(3).setField("h"), 333, 2, true)
                .on("验证level:22,2", new StringLengthValidator()
                        .setMin(2).setMax(3).setField("h"), 22, 2, true)

                .valid();

        if (result.isFailure()) {
            System.out.println(result.getFirstError().getErrorMsg());
        }

        for (ValidationError validationError : result.getErrors()) {
            System.out.println(validationError);
        }
    }
}
