package com.axinfu.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * SignUtil Tester.
 */
@SuppressWarnings("all")
public class SignUtilTest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: sort4string(Map<String, String> params)
     */
    @Test
    public void testSort4string() throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("ccc", "333");
        param.put("aaa", "111");
        param.put("bbb", "222");
        param.put("abc", "444");
        param.put("bcd", "555");
        param.put("cde", "666");

        Map<String, String> sort = SignUtil.sort4string(param);
        for (String s : sort.keySet()) {
            System.out.println(s);
        }
    }

    /**
     * Method: sort4object(Map<String, Object> params)
     */
    @Test
    public void testSort4object() throws Exception {
        Map<String, Object> param = new HashMap<>();
        param.put("ccc", "333");
        param.put("aaa", "111");
        param.put("bbb", "222");
        param.put("abc", "444");
        param.put("bcd", "555");
        param.put("cde", "666");

        Map<String, String> sort = SignUtil.sort4object(param);
        for (String s : sort.keySet()) {
            System.out.println(s);
        }
    }

    /**
     * Method: getKvStr4String(Map<String, String> params)
     */
    @Test
    public void testGetKVStr4String() throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("ccc", "333");
        param.put("aaa", "111");
        param.put("bbb", "222");
        param.put("abc", "444");
        param.put("bcd", "555");
        param.put("cde", "666");

        String str = SignUtil.getKvStr4String(param);
        System.out.println(str);
    }

    /**
     * Method: getKvStr4Object(Map<String, Object> params)
     */
    @Test
    public void testGetKVStr4Object() throws Exception {
        Map<String, Object> param = new HashMap<>();
        param.put("ccc", "333");
        param.put("aaa", "111");
        param.put("bbb", "222");
        param.put("abc", "444");
        param.put("bcd", "555");
        param.put("cde", "666");

        String str = SignUtil.getKvStr4Object(param);
        System.out.println(str);
    }
}
