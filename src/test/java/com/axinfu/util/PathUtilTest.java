package com.axinfu.util;

import com.axinfu.util.encrypt.Encryption;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * PathUtil Tester.
 */
@SuppressWarnings("all")
public class PathUtilTest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: getPackagePath(Object object)
     */
    @Test
    public void testGetPackagePathObject() throws Exception {
        System.out.println(PathUtil.getPath(new Object()));
    }

    /**
     * Method: getPath(Class clazz)
     */
    @Test
    public void testGetPathClazz() throws Exception {
        System.out.println(PathUtil.getPath(PathUtil.class));
    }

    /**
     * Method: getPath(Object object)
     */
    @Test
    public void testGetPathObject() throws Exception {
        System.out.println(PathUtil.getPath(new Encryption.EncryptionKey("str".getBytes())));
    }

    /**
     * Method: getPackagePath(Class clazz)
     */
    @Test
    public void testGetPackagePathClazz() throws Exception {
        System.out.println(PathUtil.getPackagePath(PathUtil.class));
    }

    /**
     * Method: getPackagePath(Object object)
     */
    @Test
    public void testGetPackagePath() throws Exception {
        System.out.println(PathUtil.getPackagePath(new Encryption.EncryptionKey("str".getBytes())));
    }

    /**
     * Method: getRootClassPath()
     */
    @Test
    public void testGetRootClassPath() throws Exception {
        System.out.println(PathUtil.getRootClassPath());
    }

    /**
     * Method: isAbsolutePath(String path)
     */
    @Test
    public void testIsAbsolutePath() throws Exception {
        System.out.println(PathUtil.isAbsolutePath(PathUtil.getRootClassPath()));
    }
}
