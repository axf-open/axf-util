package com.axinfu.util;

import org.junit.Test;

import java.util.Date;

/**
 * RetryUtilTest
 *
 * @author ZJN
 * @since 2022/6/15
 */
public class RetryUtilTest {

    @Test
    public void convertSeconds() {
        System.out.println(RetryUtil.convertSeconds("5s"));
        System.out.println(RetryUtil.convertSeconds("5"));
        System.out.println(RetryUtil.convertSeconds("5m"));
        System.out.println(RetryUtil.convertSeconds("5h"));
        System.out.println(RetryUtil.convertSeconds("5d"));
    }

    @Test
    public void calcNext() {
        String config = "5s|1,10s|1,15s|1,30s|1,1m|5,2m|5,5m|5,10m|5,30m|5,1h|10,2h|10,5h|10,10h|10,1d|30,30d|10";
        Date d = DateUtil.getNow();
        System.out.println(DateUtil.formatDateTime(d));
        for (int i = 0; i < 100; i++) {
            d = RetryUtil.calcNext(config, d, i);
            System.out.println(DateUtil.formatDateTime(d));
        }
    }

    @Test
    public void calcTotalNum() {
        String config = "5s|1,10s|1,15s|1,30s|1,1m|5,2m|5,5m|5,10m|5,30m|5,1h|10,2h|10,5h|10,10h|10,1d|30,30d|10";
        System.out.println(RetryUtil.calcTotalNum(config));
    }

    @Test
    public void calcTotalSeconds() {
        String config = "5s|1,10s|1,15s|1,30s|1,1m|5,2m|5,5m|5,10m|5,30m|5,1h|10,2h|10,5h|10,10h|10,1d|30,30d|10";
        System.out.println(RetryUtil.calcTotalSeconds(config));
    }
}
