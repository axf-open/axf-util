package com.axinfu.util;

import org.junit.Test;

import java.util.List;
import java.util.regex.Matcher;

/**
 * RegexUtilTest
 *
 * @author ZJN
 * @since 2022/5/7
 */
public class RegexUtilTest {
    @Test
    public void isMatch() {
        String pattern = "href=\"(update[p]?[b]?)\\?v=(.*?)\"";
        String content1 = "<a href=\"update?v=3.10.1.1R\" target=\"_blank\">3.10.1.1R</a>" +
                "<a href=\"updatep?v=3.10.1.2R\" target=\"_blank\">3.10.1.1R</a>" +
                "<a href=\"updateb?v=3.10.1.3R\" target=\"_blank\">3.10.1.1R</a>";
        String content2 = "href=\"update?v=3.10.1.1R\"";

        boolean match1 = RegexUtil.isMatch(pattern, content1);
        System.out.println(match1);

        boolean match2 = RegexUtil.isMatch(pattern, content2);
        System.out.println(match2);
    }

    @Test
    public void match() {
        String pattern = "href=\"(update[p]?[b]?)\\?v=(.*?)\"";
        String content = "<a href=\"update?v=3.10.1.1R\" target=\"_blank\">3.10.1.1R</a>" +
                "<a href=\"updatep?v=3.10.1.2R\" target=\"_blank\">3.10.1.1R</a>" +
                "<a href=\"updateb?v=3.10.1.3R\" target=\"_blank\">3.10.1.1R</a>";

        Matcher m = RegexUtil.match(pattern, content);
        while (m.find()) {
            System.out.println(m.groupCount());
            System.out.println(m.group(0));
            System.out.println(m.group(1));
            System.out.println(m.group(2));
        }
    }

    @Test
    public void matchToList() {
        String pattern = "href=\"(update[p]?[b]?)\\?v=(.*?)\"";
        String content = "<a href=\"update?v=3.10.1.1R\" target=\"_blank\">3.10.1.1R</a>" +
                "<a href=\"updatep?v=3.10.1.2R\" target=\"_blank\">3.10.1.1R</a>" +
                "<a href=\"updateb?v=3.10.1.3R\" target=\"_blank\">3.10.1.1R</a>";

        List<List<String>> rs = RegexUtil.matchToList(pattern, content);
        System.out.println(rs);
    }
}
