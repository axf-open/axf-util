package com.axinfu.util.db;

import com.alibaba.druid.pool.DruidDataSource;
import org.junit.Test;

import javax.sql.DataSource;
import java.util.List;

/**
 * DbUtilTest
 *
 * @author ZJN
 * @since 2022/5/20
 */
public class DbUtilTest {

    private static DataSource getDataSource() {
        DruidDataSource ds = new DruidDataSource();
        ds.setUrl("jdbc:mysql://192.168.0.37:7306/dev_epay");
        ds.setUsername("root");
        ds.setPassword("admin123");
        return ds;
    }

    @Test
    public void genMetaData() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource);
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_IS_SKIP_TABLE,
                DbUtil.DEFAULT_TYPES,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData1() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_IS_SKIP_TABLE,
                DbUtil.DEFAULT_TYPES
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData2() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_IS_SKIP_TABLE,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData3() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_TYPES,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData4() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_IS_SKIP_TABLE,
                DbUtil.DEFAULT_TYPES,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData5() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_IS_SKIP_TABLE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData6() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData7() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_TYPES,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData8() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_TYPES
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData9() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_IS_SKIP_TABLE,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData10() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_IS_SKIP_TABLE,
                DbUtil.DEFAULT_TYPES
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData11() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData12() {

        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData13() {

        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_DIALECT
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData14() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource,
                DbUtil.DEFAULT_HANDLE_JAVA_TYPE
        );
        System.out.println(tableMetaList.size());
    }

    @Test
    public void testGenMetaData15() {
        DataSource dataSource = getDataSource();
        List<TableMeta> tableMetaList = DbUtil.genMetaData(dataSource
        );
        System.out.println(tableMetaList.size());
    }
}
