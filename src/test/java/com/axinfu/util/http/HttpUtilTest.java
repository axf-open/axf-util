package com.axinfu.util.http;

import com.axinfu.util.http.response.StringHttpResponseResult;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * HttpUtil Tester.
 */
@SuppressWarnings("all")
public class HttpUtilTest {

    @Before
    public void before() throws Exception {
        String url = "https://www.baidu.com";
        HttpUtil.setPrintConfig(url, new HttpPrintConfig(url, true));
//        HttpUtil.setPrintConfig(url, new HttpPrintConfig(url, true, "C:\\httplog"));
    }

    @After
    public void after() throws Exception {

    }

    private Map<String, String> getQueryParams() {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("qk1", "qv1");
        return queryParams;
    }

    private Map<String, String> getHeaderParams() {
        Map<String, String> headerParams = new HashMap<>();
        headerParams.put("hk1", "hv1");
        return headerParams;
    }

    private Map<String, Object> getBodyParams() {
        Map<String, Object> bodyParams = new LinkedHashMap<>();
        bodyParams.put("bk1", "bv1");
        return bodyParams;
    }

    private byte[] getBodyParamBytes() {
        try {
            return "{\"bk1\":\"bv1\"}".getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, File> getFileMap() {
        String userDir = System.getProperty("user.dir");
        String basePath = userDir + "/src/test/resource";
        Map<String, File> fileMap = new HashMap<>();
        fileMap.put("file1", new File(basePath + "/a.txt"));
        fileMap.put("file2", new File(basePath + "/b.txt"));
        return fileMap;
    }

    private File getFile() {
        String userDir = System.getProperty("user.dir");
        String basePath = userDir + "/src/test/resource";
        return new File(basePath + "/a.txt");
    }

    /**
     * Method: getHttpClient()
     */
    @Test
    public void testGetHttpClient() throws Exception {
        System.out.println(HttpUtil.getHttpClient());
    }

    /**
     * Method: getRequestConfig()
     */
    @Test
    public void testGetRequestConfig() throws Exception {
        System.out.println(HttpUtil.getRequestConfig());
    }

    /**
     * Method: clearPrintConfig()
     */
    @Test
    public void testClearPrintConfig() throws Exception {
        HttpUtil.clearPrintConfig();
    }

    /**
     * Method: getCookieStore()
     */
    @Test
    public void testGetCookieStore() throws Exception {
        System.out.println(HttpUtil.getCookieStore());
    }

    /**
     * Method: setPrintConfig(String url, HttpPrintConfig httpPrintConfig)
     */
    @Test
    public void testSetPrintConfig() throws Exception {
        String url = "https://www.baidu.com";
        HttpUtil.setPrintConfig(url, new HttpPrintConfig(url, true));
    }

    /**
     * Method: reqeust(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String
     * httpMethod, String url, Map<String, String> queryParamMap, Map<String, String> headerMap, HttpEntity
     * bodyEntity, HttpResponseHandler responseHandler)
     */
    @Test
    public void testReqeustForHttpClientRequestConfigCookieStoreHttpMethodUrlQueryParamMapHeaderMapBodyEntityResponseHandler() throws Exception {
        String wsUrl = "https://www.baidu.com/search?a=1";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        List<NameValuePair> list = new ArrayList<>();
        for (Map.Entry<String, Object> stringStringEntry : bodyParams.entrySet()) {
            list.add(new BasicNameValuePair(stringStringEntry.getKey(), stringStringEntry.getValue().toString()));
        }
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(list);

        StringHttpResponseResult httpResponseResult = HttpUtil.reqeust(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), HttpGet.METHOD_NAME, wsUrl, queryParams,
                headerParams, urlEncodedFormEntity, null);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: reqeust(String httpMethod, String url, Map<String, String> queryParamMap, Map<String, String>
     * headerMap, HttpEntity bodyEntity, HttpResponseHandler responseHandler)
     */
    @Test
    public void testReqeustForHttpMethodUrlQueryParamMapHeaderMapBodyEntityResponseHandler() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        List<NameValuePair> list = new ArrayList<>();
        for (Map.Entry<String, Object> stringStringEntry : bodyParams.entrySet()) {
            list.add(new BasicNameValuePair(stringStringEntry.getKey(), stringStringEntry.getValue().toString()));
        }
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(list);

        StringHttpResponseResult httpResponseResult = HttpUtil.reqeust(HttpGet.METHOD_NAME, wsUrl, queryParams,
                headerParams, urlEncodedFormEntity, null);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: reqeust(String httpMethod, String url, Map<String, String> queryParamMap, Map<String, String>
     * headerMap, HttpEntity bodyEntity, ResponseHandler<? extends HttpResponseResult> responseHandler, int
     * connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testReqeustForHttpMethodUrlQueryParamMapHeaderMapBodyEntityResponseHandlerConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        List<NameValuePair> list = new ArrayList<>();
        for (Map.Entry<String, Object> stringStringEntry : bodyParams.entrySet()) {
            list.add(new BasicNameValuePair(stringStringEntry.getKey(), stringStringEntry.getValue().toString()));
        }
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(list);

        StringHttpResponseResult httpResponseResult = HttpUtil.reqeust(HttpGet.METHOD_NAME, wsUrl, queryParams,
                headerParams, urlEncodedFormEntity, null,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: get(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testGetForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.get(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: get(String url, Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testGetForUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.get(wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: get(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, int
     * connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testGetForUrlQueryParamMapHeaderMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.get(wsUrl, queryParams, headerParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object> bodyParamMap)
     */
    @Test
    public void testPostForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams, bodyParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap, int connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {

        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, byte[] bodyParamBytes)
     */
    @Test
    public void testPostForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamBytes() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams,
                bodyParamBytes);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, byte[] bodyParamBytes)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamBytes() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParamBytes);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, byte[]
     * bodyParamBytes, int connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamBytesConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParamBytes,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object> bodyParamMap,
     * Map<String, File> fileMap)
     */
    @Test
    public void testPostForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamMapFileMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        Map<String, File> fileMap = getFileMap();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams, bodyParams,
                fileMap);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap, Map<String, File> fileMap)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamMapFileMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        Map<String, File> fileMap = getFileMap();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParams,
                fileMap);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap, Map<String, File> fileMap, int connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamMapFileMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        Map<String, File> fileMap = getFileMap();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParams,
                fileMap,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, String bodyParamName, byte[] bodyParamBytes,
     * Map<String, File> fileMap)
     */
    @Test
    public void testPostForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamNameBodyParamBytesFileMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        Map<String, File> fileMap = getFileMap();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams, "json",
                bodyParamBytes, fileMap);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, String
     * bodyParamName, byte[] bodyParamBytes, Map<String, File> fileMap)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamNameBodyParamBytesFileMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        Map<String, File> fileMap = getFileMap();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, "json",
                bodyParamBytes, fileMap);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, String
     * bodyParamName, byte[] bodyParamBytes, Map<String, File> fileMap, int connectionRequestTimeout, int
     * connectTimeout, int socketTimeout)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamNameBodyParamBytesFileMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        Map<String, File> fileMap = getFileMap();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, "json",
                bodyParamBytes, fileMap,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object> bodyParamMap, String
     * fileName, File file)
     */
    @Test
    public void testPostForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamMapFileNameFile() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        String fileName = "file1";
        File file = getFile();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams, bodyParams,
                fileName, file);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap, String fileName, File file)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamMapFileNameFile() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        String fileName = "file1";
        File file = getFile();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParams,
                fileName, file);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap, String fileName, File file, int connectionRequestTimeout, int connectTimeout, int
     * socketTimeout)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamMapFileNameFileConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        String fileName = "file1";
        File file = getFile();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, bodyParams,
                fileName, file,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, String bodyParamName, byte[] bodyParamBytes,
     * String fileName, File file)
     */
    @Test
    public void testPostForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamNameBodyParamBytesFileNameFile() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        String fileName = "file1";
        File file = getFile();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams, "json",
                bodyParamBytes, fileName, file);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, String
     * bodyParamName, byte[] bodyParamBytes, String fileName, File file)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamNameBodyParamBytesFileNameFile() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        String fileName = "file1";
        File file = getFile();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, "json",
                bodyParamBytes, fileName, file);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: post(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, String
     * bodyParamName, byte[] bodyParamBytes, String fileName, File file, int connectionRequestTimeout, int
     * connectTimeout, int socketTimeout)
     */
    @Test
    public void testPostForUrlQueryParamMapHeaderMapBodyParamNameBodyParamBytesFileNameFileConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        String fileName = "file1";
        File file = getFile();

        StringHttpResponseResult httpResponseResult = HttpUtil.post(wsUrl, queryParams, headerParams, "json",
                bodyParamBytes, fileName, file,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: put(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object> bodyParamMap)
     */
    @Test
    public void testPutForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.put(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams, bodyParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: put(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap)
     */
    @Test
    public void testPutForUrlQueryParamMapHeaderMapBodyParamMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.put(wsUrl, queryParams, headerParams, bodyParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: put(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object>
     * bodyParamMap, int connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testPutForUrlQueryParamMapHeaderMapBodyParamMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.put(wsUrl, queryParams, headerParams, bodyParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: put(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, byte[] bodyParamBytes)
     */
    @Test
    public void testPutForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamBytes() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.put(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams,
                bodyParamBytes);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: put(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, byte[] bodyParamBytes)
     */
    @Test
    public void testPutForUrlQueryParamMapHeaderMapBodyParamBytes() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.put(wsUrl, queryParams, headerParams, bodyParamBytes);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: put(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, byte[]
     * bodyParamBytes, int connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testPutForUrlQueryParamMapHeaderMapBodyParamBytesConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.put(wsUrl, queryParams, headerParams, bodyParamBytes,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: head(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testHeadForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.head(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: head(String url, Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testHeadForUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.head(wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: head(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, int
     * connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testHeadForUrlQueryParamMapHeaderMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.head(wsUrl, queryParams, headerParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: delete(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testDeleteForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.delete(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: delete(String url, Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testDeleteForUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.delete(wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: delete(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, int
     * connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testDeleteForUrlQueryParamMapHeaderMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.delete(wsUrl, queryParams, headerParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: patch(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String, Object> bodyParamMap)
     */
    @Test
    public void testPatchForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.patch(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams, bodyParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: patch(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String,
     * Object> bodyParamMap)
     */
    @Test
    public void testPatchForUrlQueryParamMapHeaderMapBodyParamMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.patch(wsUrl, queryParams, headerParams, bodyParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: patch(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, Map<String,
     * Object> bodyParamMap, int connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testPatchForUrlQueryParamMapHeaderMapBodyParamMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        Map<String, Object> bodyParams = getBodyParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.patch(wsUrl, queryParams, headerParams, bodyParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: patch(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap, byte[] bodyParamBytes)
     */
    @Test
    public void testPatchForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMapBodyParamBytes() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.patch(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams,
                bodyParamBytes);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: patch(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, byte[]
     * bodyParamBytes)
     */
    @Test
    public void testPatchForUrlQueryParamMapHeaderMapBodyParamBytes() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.patch(wsUrl, queryParams, headerParams, bodyParamBytes);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: patch(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, byte[]
     * bodyParamBytes, int connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testPatchForUrlQueryParamMapHeaderMapBodyParamBytesConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        byte[] bodyParamBytes = getBodyParamBytes();

        StringHttpResponseResult httpResponseResult = HttpUtil.patch(wsUrl, queryParams, headerParams, bodyParamBytes,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: options(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testOptionsForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.options(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: options(String url, Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testOptionsForUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.options(wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: options(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, int
     * connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testOptionsForUrlQueryParamMapHeaderMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.options(wsUrl, queryParams, headerParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: trace(HttpClient httpClient, RequestConfig requestConfig, CookieStore cookieStore, String url,
     * Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testTraceForHttpClientRequestConfigCookieStoreUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.trace(HttpUtil.getHttpClient(),
                HttpUtil.getRequestConfig(), HttpUtil.getCookieStore(), wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: trace(String url, Map<String, String> queryParamMap, Map<String, String> headerMap)
     */
    @Test
    public void testTraceForUrlQueryParamMapHeaderMap() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.trace(wsUrl, queryParams, headerParams);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: trace(String url, Map<String, String> queryParamMap, Map<String, String> headerMap, int
     * connectionRequestTimeout, int connectTimeout, int socketTimeout)
     */
    @Test
    public void testTraceForUrlQueryParamMapHeaderMapConnectionRequestTimeoutConnectTimeoutSocketTimeout() throws Exception {
        String wsUrl = "https://www.baidu.com";

        Map<String, String> queryParams = getQueryParams();

        Map<String, String> headerParams = getHeaderParams();

        StringHttpResponseResult httpResponseResult = HttpUtil.trace(wsUrl, queryParams, headerParams,
                3000,
                3000,
                3000);
        System.out.println(httpResponseResult.getResponseStr());
    }

    /**
     * Method: getQueryString(Map<String, String> queryParamMap, boolean isEncode)
     */
    @Test
    public void testGetQueryStringForQueryParamMapIsEncode() throws Exception {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("qk1", "qv1");
        queryParams.put("qk2", "qv2");
        System.out.println(HttpUtil.getQueryString(queryParams, true));
    }

    /**
     * Method: getQueryString(Map<String, String> queryParamMap)
     */
    @Test
    public void testGetQueryStringQueryParamMap() throws Exception {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("qk1", "qv1");
        queryParams.put("qk2", "qv2");
        System.out.println(HttpUtil.getQueryString(queryParams));
    }

    /**
     * Method: urlBuildQueryString(String url, Map<String, String> queryParamMap, boolean isEncode)
     */
    @Test
    public void testUrlBuildQueryStringForUrlQueryParamMapIsEncode() throws Exception {
        String wsUrl = "https://www.baidu.com";
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("qk1", "qv1");
        queryParams.put("qk2", "qv2");
        System.out.println(HttpUtil.urlBuildQueryString(wsUrl, queryParams, true));
    }

    /**
     * Method: urlBuildQueryString(String url, Map<String, String> queryParamMap)
     */
    @Test
    public void testUrlBuildQueryStringForUrlQueryParamMap() throws Exception {
        String wsUrl = "https://www.baidu.com";
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("qk1", "qv1");
        queryParams.put("qk2", "qv2");
        System.out.println(HttpUtil.urlBuildQueryString(wsUrl, queryParams));
    }

    /**
     * Method: parseUrl(String url)
     */
    @Test
    public void testParseUrl() throws Exception {
        String wsUrl = "https://www.baidu.com";
        System.out.println(HttpUtil.parseUrl(wsUrl));
    }
}
