package com.axinfu.util.filescanner;

import com.axinfu.util.FileUtil;
import com.axinfu.util.PathUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * FileScanner Tester.
 */
@SuppressWarnings("all")
public class FileScannerTest {

    private static String jarPath = "D:\\workspace\\resource\\maven_repository\\com\\axinfu\\axf-util" +
            "\\1.2.4R\\axf-util-1.2.4R.jar";

    private static List<java.io.File> fileJars = new ArrayList<java.io.File>() {
        {
            add(new java.io.File("D:\\workspace\\resource\\maven_repository" +
                    "\\com\\axinfu\\axf-util\\1.2.4R\\axf-util-1.2.4R.jar"));
            add(new java.io.File("D:\\workspace\\resource\\maven_repository" +
                    "\\junit\\junit\\4.12\\junit-4.12.jar"));
            add(new java.io.File("D:\\workspace\\resource\\maven_repository" +
                    "\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        }
    };

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void findDirs() {
        List<java.io.File> dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", false, 0, 1);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
        System.out.println("+++++++++++++++++++++++++++++");
        dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", true, 0, 1);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
        System.out.println("+++++++++++++++++++++++++++++");
        dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", true, 2, 1);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    @Test
    public void testFindDirs1() {
        List<java.io.File> dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", true, 1);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
        System.out.println("+++++++++++++++++++++++++++++");
        dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", true, 2);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    /**
     * Method: findDirs(String baseDirPath, String matchesStr, boolean scanChildDir)
     */
    @Test
    public void testFindDirsForBaseDirPathMatchesStrScanChildDir() throws Exception {
        List<java.io.File> dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", false);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
        System.out.println("+++++++++++++++++++++++++++++");
        dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", true);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    @Test
    public void testFindDirs2() {
        List<java.io.File> dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*", 2);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    /**
     * Method: findDirs(String baseDirPath, String matchesStr)
     */
    @Test
    public void testFindDirsForBaseDirPathMatchesStr() {
        List<java.io.File> dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*");
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    @Test
    public void testFindDirs3() {
        List<java.io.File> dirs = FileScanner.findDirs(".*", 2);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    /**
     * Method: findDirs(String matchesStr)
     */
    @Test
    public void testFindDirsMatchesStr() {
        List<java.io.File> dirs = FileScanner.findDirs(".*");
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    @Test
    public void testFindDirs4() {
        List<java.io.File> dirs = FileScanner.findDirs(2);
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    /**
     * Method: findDirs()
     */
    @Test
    public void testFindDirs() {
        List<java.io.File> dirs = FileScanner.findDirs();
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    @Test
    public void findFiles() {
        List<java.io.File> files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", false, 0, 1);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
        System.out.println("++++++++++++++++++++++++++");
        files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", true, 0, 1);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
        System.out.println("++++++++++++++++++++++++++");
        files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", true, 4, 1);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFiles1() {
        List<java.io.File> files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", false, 0);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
        System.out.println("++++++++++++++++++++++++++");
        files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", true, 0);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
        System.out.println("++++++++++++++++++++++++++");
        files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", true, 3);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFiles(String baseDirPath, String matchesStr, boolean scanChildDir)
     */
    @Test
    public void testFindFilesForBaseDirPathMatchesStrScanChildDir() {
        List<java.io.File> files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", true);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFiles2() {
        List<java.io.File> files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", 4);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFiles(String baseDirPath, String matchesStr)
     */
    @Test
    public void testFindFilesForBaseDirPathMatchesStr() {
        List<java.io.File> files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*");
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFiles3() {
        List<java.io.File> files = FileScanner.findFiles(".*", 4);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFiles(String matchesStr)
     */
    @Test
    public void testFindFilesMatchesStr() {
        List<java.io.File> files = FileScanner.findFiles(".*");
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFiles4() {
        List<java.io.File> files = FileScanner.findFiles(4);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFiles()
     */
    @Test
    public void testFindFiles() {
        List<java.io.File> files = FileScanner.findFiles();
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void findFilesFromJar() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath), ".*", ".*", 0);
        for (File file : files) {
            System.out.println(file.toString());
        }
        System.out.println("++++++++++++++");
        files = FileScanner.findFilesFromJar(new java.io.File(jarPath), ".*", ".*", 3);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJar(java.io.File fileJar, String matchesStr4Package, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarForFileJarMatchesStr4PackageMatchesStr4File() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath), ".*", ".*");
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFilesFromJar1() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath), ".*", 3);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFilesFromJar() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath), ".*");
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void findFilesFromJars() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath), ".*", 3);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJar(java.io.File fileJar, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarForFileJarMatchesStr4File() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath), ".*");
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFilesFromJars() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath), 3);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJar(java.io.File fileJar)
     */
    @Test
    public void testFindFilesFromJarFileJar() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File(jarPath));
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void testFindFilesFromJars1() {
        List<File> files = FileScanner.findFilesFromJars(fileJars, "org.junit.*", ".*", 3);
        for (File file : files) {
            System.out.println(file.toString());
            InputStream is = this.getClass().getResourceAsStream(file.getPath());
            System.out.println(FileUtil.readToString(is));
        }
    }

    /**
     * Method: findFilesFromJars(List<java.io.File> filesJar, String matchesStr4Package, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarsForFilesJarMatchesStr4PackageMatchesStr4File() {
        List<File> files = FileScanner.findFilesFromJars(fileJars, "org.junit.*", ".*");
        for (File file : files) {
            System.out.println(file.toString());
            InputStream is = this.getClass().getResourceAsStream(file.getPath());
            System.out.println(FileUtil.readToString(is));
        }
    }

    @Test
    public void testFindFilesFromJars2() {
        List<File> files = FileScanner.findFilesFromJars(fileJars, ".*", 3);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJars(List<java.io.File> filesJar, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarsForFilesJarMatchesStr4File() {
        List<File> files = FileScanner.findFilesFromJars(fileJars, ".*");
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJars(List<java.io.File> filesJar)
     */
    @Test
    public void testFindFilesFromJars3() {
        List<File> files = FileScanner.findFilesFromJars(fileJars, 3);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJars(List<java.io.File> filesJar)
     */
    @Test
    public void testFindFilesFromJarsFilesJar() {
        List<File> files = FileScanner.findFilesFromJars(fileJars);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    @Test
    public void findClass() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*", ".*", Object.class, true, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4Package, String
     * matchesStr4File, Class parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4PackageMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*", ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*", Object.class, true, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4File, Class
     * parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass1() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*", Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4File, Class
     * parentClass)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass2() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*", 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4File)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4File() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(),
                ".*");
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass3() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), ".*",
                Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassForBaseDirPathPreMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), ".*",
                Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass4() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), ".*", 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre, String matchesStr4File)
     */
    @Test
    public void testFindClassForBaseDirPathPreMatchesStr4File() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), ".*");
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass5() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(),
                Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre, Class parentClass)
     */
    @Test
    public void testFindClassForBaseDirPathPreParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(),
                Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass6() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre)
     */
    @Test
    public void testFindClassForBaseDirPathPre() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath());
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass7() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String pre, Class parentClass)
     */
    @Test
    public void testFindClassForPreParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClass8() {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String pre)
     */
    @Test
    public void testFindClassPre() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath());
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void findClassFromJar() {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), ".*", ".*", Object.class, true,
                3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, String matchesStr4Package, String matchesStr4File, Class
     * parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassFromJarForFileJarMatchesStr4PackageMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), ".*", ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJar() {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), ".*", ".*", Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, String matchesStr4Package, String matchesStr4File, Class
     * parentClass)
     */
    @Test
    public void testFindClassFromJarForFileJarMatchesStr4PackageMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), ".*", ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJar1() {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), ".*", Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassFromJarForFileJarMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJar2() {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, Class parentClass)
     */
    @Test
    public void testFindClassFromJarForFileJarParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJar3() {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath), 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar)
     */
    @Test
    public void testFindClassFromJarFileJar() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File(jarPath));
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void findClassFromJars() {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", ".*", Object.class, true, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, String matchesStr4Package, String matchesStr4File,
     * Class parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassFromJarsForFilesJarMatchesStr4PackageMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJars() {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", ".*", Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, String matchesStr4Package, String matchesStr4File,
     * Class parentClass)
     */
    @Test
    public void testFindClassFromJarsForFilesJarMatchesStr4PackageMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJars1() {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassFromJarsForFilesJarMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJars2() {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, Object.class, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, Class parentClass)
     */
    @Test
    public void testFindClassFromJarsForFilesJarParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    @Test
    public void testFindClassFromJars3() {
        List<Class> classes = FileScanner.findClassFromJars(fileJars, 3);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar)
     */
    @Test
    public void testFindClassFromJarsFilesJar() throws Exception {
        List<Class> classes = FileScanner.findClassFromJars(fileJars);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }
}
