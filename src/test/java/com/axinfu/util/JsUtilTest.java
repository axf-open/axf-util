package com.axinfu.util;

import com.axinfu.util.enhance.JsonQ;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * JsUtilTest
 *
 * @author ZJN
 * @since 2022/5/13
 */
public class JsUtilTest {

    @Test
    public void escapeString() {
        String s = "[社会培训]ISO9000\\ISO22000\\GMP培\\n训";
        System.out.println(s);
        System.out.println(JsUtil.escapeString(s));
    }

    @Test
    public void escapeJsonQ() {
        String s = "[社会培训]ISO9000\\ISO22000\\GMP培\\n训";
        JsonQ jsonQ = JsonQ.by("s", s);
        JsUtil.escapeJsonQ(jsonQ);
        System.out.println(jsonQ.get("s"));
    }

    @Test
    public void escapeObjectMap() {
        String s = "[社会培训]ISO9000\\ISO22000\\GMP培\\n训";
        Map<String, Object> map = new HashMap<>();
        map.put("s", s);
        JsUtil.escapeObjectMap(map);
        System.out.println(map.get("s"));
    }

    @Test
    public void escapeStringMap() {
        String s = "[社会培训]ISO9000\\ISO22000\\GMP培\\n训";
        Map<String, String> map = new HashMap<>();
        map.put("s", s);
        JsUtil.escapeStringMap(map);
        System.out.println(map.get("s"));
    }
}
