package com.axinfu.util.validjsonq.cache;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

/**
 * ValidJsonQCache Tester.
 */
@SuppressWarnings("all")
public class ValidJsonQCacheTest {

    @Test
    public void test() throws Exception {
        ValidJsonQCache.INSTANCE.setCacheLoader(new DefaultCacheLoader());
        JSONObject demoJson = ValidJsonQCache.INSTANCE.get("demo");
        System.out.println(demoJson);
    }
}
