# axf-util

#### 项目说明

> 安心付常用工具包（使用示例建test包测试用例）

#### 使用方式

```
<dependency>
    <groupId>com.axinfu</groupId>
    <artifactId>axf-util</artifactId>
    <version>1.2.7R</version>
</dependency>
```

#### 升级记录

```
1.2.7R
1、FileScanner修复一处bug

1.2.6R
1、FileScanner修复一处bug

1.2.5R
1、FileScanner增加层级的支持

1.2.4R
1、RetryUtil抛异常优化

1.2.3R
1、增加RetryUtil，简单的重试机制实现工具类

1.2.2R
1、EmptyUtil部分方法泛型优化

1.2.1R
1、EmptyUtil消除语义歧义

1.1.5R
1、完善java类型映射
1、ColumnMeta添加常用扩展属性

1.1.4R
1、fix：使TableMeta的columnMetas可见

1.1.2R
1、TableMeta、ColumnMeta继承JsonQ使属性可扩展

1.1.1R
1、采用语义版本号管理
2、StringUtil增加join系列方法
3、增加DbUtil，获取数据库表、字段等信息，可用于代码生成

1.0.19R
1、修复HttpUtil使用了全局变量引发的多线程问题

1.0.18R
1、增加JsUtil（对js转义符号的处理）

1.0.17R
1、增加RegexUtil

1.0.16R
1、增加ConstantObject
2、增加FinalObject

1.0.15R
1、增大HttpUtil默认超时时间

1.0.14R
1、FileScanner.findDirs增加重构，支持指定是否扫描子文件夹

1.0.13R
1、StringUtil增加下划线驼峰互转

1.0.12R
1、JsonQ增加toMapString

1.0.11R
1、JsonQ增加toMap

1.0.10R
1、修复IsNotEmptyValidator判断失误，优化validator去掉泛型避免类型转换出错
2、增加空值三元表达式函数

1.0.9R
1、增加btgset

1.0.8R
1、HttpUtil扩充一些常用方法
2、代码使用阿里规约检查进行规范
3、jdk基于1.8（泛型、lambda）
4、增加validq、validjsonq

1.0.7R
1、StringUtil添加addLastSuffix方法用于处理最后一个后缀

1.0.6R
1、HttpUtil修复cleanDir在文件夹非空时清除失败

1.0.5R
1、HttpUtil打印日志配置，强制匹配策略调整为url以设置url开头进行匹配

1.0.4R
1、工具类尽量不依赖日志框架，有错误直接抛出

1.0.3R
1、HttpUtil默认返回header转小写

1.0.2R
1、重构HttpUtil，兼容以前的使用方式（上一版的方式使用的地方改动太多，另外其实可以使用fluent-hc的，但是其对httpclient、requestConfig等的扩展性较弱，所以还是选择自己写）

1.0.1R
1、首次发布
```

---
